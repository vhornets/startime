<?php
/**
 * Template Name: Recording songs 2
 */
?>

<?php get_header(); ?>

<?php if(have_posts()): the_post(); ?>

<section class="c-section-hero">
    <div class="c-slider-hero__images">
        <img src="<?php echo get_field('post-hero__image')['url']; ?>" alt="" class="c-slider-hero__image">
        <img src="<?php bloginfo('template_url'); ?>/img/templates/news/news_bg_top.png" class="news_bg_1">
    </div>

    <div class="l-container">
        <div class="c-slider-hero">
            <div class="c-slider-hero__slide">
                <h1 class="c-slider-hero__title margin-none">
                    <?php the_field('post-hero__title'); ?>
                </h1>

                <h3 class="c-slider-hero__sub-title">
                    <?php the_field('post-hero__subtitle'); ?>
                </h3>
                <div class="o-badge c-slider-hero__badge">наши новости / <?php echo get_the_date( 'd.m.y' ); ?></div>
            </div>
        </div>
    </div>
</section>

<section class="c-section-about-course">
    <img src="<?php bloginfo('template_url'); ?>/img/templates/teleprojects/about_bg.svg" class="c-section-about-course__bg" />

    <div class="l-about-course">
        <div class="c-about-camp">
            <p class="c-about-camp__date"><?php echo get_the_date( 'd.m.y' ); ?></p>

            <div class="wysiwyg">
                <?php the_content(); ?>
            </div>
        </div>
    </div>
</section>

<section class="c-section-photos">
    <h2>Фотографии</h2>
<!--    --><?php //get_template_part('partials/gallery'); ?>
</section>

<?php $featuredPosts = get_posts([
    'posts_per_page' => 4,
    'exclude' => get_the_ID()
]); ?>

<section class="c-section-news">
    <h2>Другие новости</h2>
    <div class="l-container">
        <div class="news-block">
            <div class="news-block__data">
                <?php foreach($featuredPosts as $post): setup_postdata($post); ?>
                    <?php get_template_part('partials/news-card'); ?>
                <?php endforeach; wp_reset_postdata(); ?>
            </div>

            <a href="<?php echo get_the_permalink( get_option('page_for_posts') ); ?>" class="o-button-default c-slider-hero__button">
                Показать еше
            </a>
        </div>
    </div>
</section>

<?php endif; ?>
<script type="application/ld+json">
{
"@context": "http://schema.org",
"@type": "Article",
"mainEntityOfPage": {
    "@type": "WebPage",
    "@id": "(URL-адрес статьи)"
  },
"headline": "<?php the_title(); ?>",
"image": [
"<?php the_post_thumbnail_url(); ?>"
],
"datePublished": "<?php the_time('c'); ?>",
"dateModified": "<?php the_modified_date('c'); ?>",
"author": {
"@type": "Person",
"name": "<?=get_the_author(); ?>"
},
"publisher": {
"@type": "Organization",
"name": "<?=get_the_author(); ?>",
"logo": {
"@type": "ImageObject",
"url": "http://startime.devpro.agency/wp-content/themes/startime/img/startime-logo.svg"
}
},
"description": "<?=wp_strip_all_tags(str_replace('"', '', get_the_content())); ?>"
}
</script>
<?php get_footer(); ?>

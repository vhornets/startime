<section class="c-section-contacts">
    <div class="l-contacts">
        <div class="l-contacts__column">
            <div class="c-contacts__section">
                <div class="c-contacts__title c-contacts__title--mb">
                    Наши контакты
                </div>

                <?php foreach(get_field('phones-additional', 'options') as $phone): ?>
                    <a href="tel:<?php echo $phone['phone']; ?>" class="c-contacts__link c-contacts__link--bold">
                        <?php echo $phone['phone']; ?>
                    </a>
                <?php endforeach; ?>

                <div class="c-contacts__link c-contacts__link--address">
                    <?php the_field('address', 'options'); ?>
                </div>
            </div>

            <div class="c-contacts__section">
                <div class="c-contacts__title c-contacts__title--mb">
                    График работы
                </div>

                <div class="c-contacts__link c-contacts__link--bold">
                    <?php the_field('schedule', 'options'); ?>
                </div>
            </div>
            <div class="c-contacts__section">
                <div class="c-contacts__title c-contacts__title--mb">
                    <a href="/карта сайта/">Карта сайта</a>
                </div>
            </div>
        </div>

        <div class="l-contacts__column">
            <?php foreach(get_field('emails', 'options') as $email): ?>
                <div class="c-contacts__section">
                    <div class="c-contacts__title c-contacts__title--small">
                        <?php echo $email['title']; ?>
                    </div>

                    <a href="mailto:<?php echo $email['email']; ?>" class="c-contacts__link">
                        <?php echo $email['email']; ?>
                    </a>
                </div>
            <?php endforeach; ?>
        </div>

        <div class="c-contacts__form">
            <form class="c-form js-form-validate js-form-submit">
                <div class="c-form__title">
                    Свяжитесь с нами
                </div>

                <div class="c-form__group">
                    <input type="text" name="name" class="c-form__control" placeholder="Ваше имя" required />
                </div>

                <div class="c-form__group">
                    <input type="tel" name="tel" class="c-form__control" placeholder="Ваш телефон" required />
                </div>

                <div class="c-form__group">
                    <textarea name="message" class="c-form__control" placeholder="Сообщение"></textarea>
                </div>

                <div class="c-form__group c-form__group--submit">
                    <button type="submit" class="o-button-default c-form__submit" data-submitting-text="<?php _e('Отправка'); ?>...">
                        Отправить
                    </button>
                </div>

                <div class="c-form__group c-form__group--mb0">
                    <div class="c-checkbox">
                        <input name="agreement" type="checkbox" class="c-checkbox__input" id="form-agreement" value="1" required />
                        <label class="c-checkbox__label" for="form-agreement">Я даю свое согласие на обработку персональных данных</label>
                    </div>
                </div>

                <?php wp_nonce_field('send_request'); ?>
                <input type="hidden" name="action" value="sendRequest">
            </form>
        </div>
    </div>

    <div class="c-map" id="map" data-map-marker="<?php echo htmlspecialchars(json_encode( get_field('map', 'options') ), ENT_QUOTES, 'UTF-8'); ?>"></div>
</section>

<div class="c-footer">
    <div class="l-footer">
        <div class="l-footer__left">
            <ul class="c-socials c-socials--minimal c-socials--white">
                <?php get_template_part('partials/socials'); ?>
            </ul>
        </div>

        <div class="l-footer__right">
            <img src="<?php bloginfo('template_url'); ?>/img/startime-logo.svg" alt="" class="c-footer__logo">
            <img src="<?php bloginfo('template_url'); ?>/img/happytime-logo.png" alt="" class="c-footer__logo">
        </div>

        <div class="l-footer__copyright">
            © ООО "Стар Тайм" 2009 - <?php echo date('Y'); ?>
        </div>
    </div>
</div>

<div class="remodal" data-remodal-id="modal-response">
    <button data-remodal-action="close" class="remodal-close">
        <i class="icon svg-close-modal svg-close-modal-dims"></i>
    </button>

    <h2 class="c-modal__content"></h2>
</div>

<script type="application/ld+json">
{ "@context" : "http://schema.org",
  "@type" : "Organization",
  "name" : "Продюсерский центр Star Time",
  "url" : "https://startime.ua",
  "sameAs" : [
    "https://www.facebook.com/StarTimeUA",
    "https://www.youtube.com/channel/UCQU6mfg23peGwwTpA_5-RyA",
    "https://www.instagram.com/startime.ua/"
]
}
</script>

<script type="application/ld+json">
{
    "@context": "http://schema.org/",
    "@type": "Organization",
    "url": "https://startime.ua",
    "logo": "http://startime.zstudio.site/wp-content/themes/startime/img/startime-logo.svg"
}
</script>

<?php wp_footer(); ?>
</body>
</html>

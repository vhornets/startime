<?php
get_header();

$term = get_queried_object();
?>

<section class="c-section-main-bg">
    <img src="<?php echo get_field('project-category__bg', $term)['url']; ?>" class="camp_main__img" />
    <img src="<?php bloginfo('template_url'); ?>/img/templates/camp/camp_main_photo_bg.svg" class="camp_main__bg" />

    <div class="l-position text-center">
        <div class="l-container text-color">
            <h2 class="c-section-main-bg__title">
                Продюсерский центр
            </h2>

            <?php wp_nav_menu(array(
                'menu' => 'archive-project-menu',
                'theme_location' => 'archive-project-menu',
                'container' => false,
                'menu_class' => 'c-tabs-nav tabs-mob tabs-mob__col-3',
            )); ?>
        </div>
    </div>
</section>

<section class="c-section-producing">
    <img src="<?php bloginfo('template_url'); ?>/img/templates/producing/producing_bg.svg" class="c-section-producing__bg" />
    <div class="c-section-producing__container">
        <?php if(have_posts()): while(have_posts()): the_post(); ?>
            <?php if(get_field('project-category__post-template', $term) == 'plain'): ?>
                <article class="producing__single">
                    <img src="<?php echo get_thumbnail_src('medium_large') ?>" title="<?php the_title(); ?>" alt="<?php the_title(); ?>">

                    <div class="producing__single_data">
                        <h6><?php the_title(); ?></h6>

                        <p><?php echo strip_tags(get_the_content(false)); ?></p>

                        <a href="<?php the_permalink(); ?>" class="c-section-producing__button">
                            <?php _e('Подробнее', 'startime'); ?>
                        </a>
                    </div>
                </article>
            <?php else: ?>
                <a href="<?php the_permalink(); ?>" class="producing__single producing__single--musicals">
                    <img src="<?php echo get_thumbnail_src('medium_large') ?>" title="<?php the_title(); ?>" alt="<?php the_title(); ?>">

                    <div class="producing__single_data">
                        <h6><?php the_title(); ?></h6>

                        <p><?php echo strip_tags(get_the_content(false)); ?></p>
                    </div>
                </a>
            <?php endif; ?>
        <?php endwhile; endif; ?>
    </div>
</section>


<?php get_footer(); ?>

<?php
/**
 * Template Name: Вакансии
 */
?>

<?php get_header(); ?>

<section class="c-section-main-bg">
    <img src="<?php echo get_field('faq-head__image')['url']; ?>" class="camp_main__img" />
    <img src="<?php bloginfo('template_url'); ?>/img/templates/news/news_bg_top.png" class="camp_main__bg" />

    <div class="l-position text-center">
        <div class="l-container text-color">
            <h2 class="c-section-main-bg__title">
               <?php the_field('faq-head__title'); ?>
            </h2>

            <button class="o-button-default o-button-default--purple open-popup" data-open="entry" data-section="<?php echo strip_tags(get_field('faq-head__title')); ?>">
                <?php the_field('faq-head__button-text'); ?>
            </button>
        </div>
    </div>
</section>

<section class="c-section-vacancy">
    <img src="<?php bloginfo('template_url'); ?>/img/templates/vacancy/vacancy_bg.svg" class="c-section-vacancy__bg" />

    <div class="c-section-vacancy__container">
        <button class="arrow-btn arrow-btn--left c-slider-teachers__arrow c-slider-news__arrow--prev js-change-slide" data-slider="#slider-vacancy" data-action="slickPrev">
            <?php echo file_get_contents( get_bloginfo('template_url') . '/img/templates/vacancy/arrow.svg' ); ?>
        </button>

        <div class="c-slider-vacancy" id="slider-vacancy">
            <?php foreach(get_field('vacancies') as $vacancy): ?>
                <div class="c-slider-vacancy__slide">
                    <?php echo $vacancy['text']; ?>
                </div>
            <?php endforeach; ?>
        </div>

        <button class="arrow-btn c-slider-teachers__arrow c-slider-news__arrow--next js-change-slide" data-slider="#slider-vacancy" data-action="slickNext">
            <?php echo file_get_contents( get_bloginfo('template_url') . '/img/templates/vacancy/arrow.svg' ); ?>
        </button>
    </div>

    <div class="slide-nav-arrows visible--mobile" style="margin-top: 30px;">
        <button class="slide-nav-mob slide-nav-mob--prev js-change-slide" data-slider="#slider-vacancy" data-action="slickPrev">
            <img src="<?= get_bloginfo('template_url') . '/img/templates/slider-arrow.svg'; ?>">
        </button>

        <button class="slide-nav-mob slide-nav-mob--next js-change-slide" data-slider="#slider-vacancy" data-action="slickNext">
            <img src="<?= get_bloginfo('template_url') . '/img/templates/slider-arrow.svg'; ?>">
        </button>
    </div>
</section>

<section class="c-section-reasons">
    <?php get_template_part('partials/course-reasons'); ?>
</section>

<section class="c-section-work">
    <img src="<?php bloginfo('template_url'); ?>/img/templates/vacancy/work__bg.svg" class="c-section-work__bg">

    <div class="c-section-work__container">
        <div class="c-section-work__image">
            <img src="<?php echo get_field('vacancies-schedule__image')['url']; ?>">
        </div>

        <div class="c-section-work__data">
            <p><?php the_field('vacancies-schedule__title'); ?></p>

            <p class="bold"><?php the_field('vacancies-schedule__subtitle'); ?></p>
        </div>
    </div>
</section>

<section class="c-section-why">
    <div class="l-container text-center">
        <h2><?php the_field('vacancies-why__title'); ?></h2>
    </div>

    <div class="c-section-why__container hidden--mobile">
        <?php foreach(get_field('vacancies-why__cards') as $card): ?>
            <div class="c-card-why__item text-center">
                <div class="c-card-why__image">
                    <img src="<?php echo $card['image']['url']; ?>" title="<?php echo $card['title']; ?>" alt="<?php echo $card['title']; ?>">
                </div>

                <div class="c-card-why__title">
                    <?php echo $card['title']; ?>
                </div>

                <p class="c-card-why__text">
                    <?php echo $card['text']; ?>
                </p>
            </div>
        <?php endforeach; ?>
    </div>
    <div class="c-slider-news visible--mobile" id="slider-card-why">
        <?php foreach(get_field('vacancies-why__cards') as $card): ?>
            <div class="c-card-why__item text-center">
                <div class="c-card-why__image">
                    <img src="<?php echo $card['image']['url']; ?>" title="<?php echo $card['title']; ?>" alt="<?php echo $card['title']; ?>">
                </div>

                <div class="c-card-why__title">
                    <?php echo $card['title']; ?>
                </div>

                <p class="c-card-why__text">
                    <?php echo $card['text']; ?>
                </p>
            </div>
        <?php endforeach; ?>
    </div>
    <div class="slide-nav-arrows visible--mobile">
        <button class="slide-nav-mob slide-nav-mob--prev js-change-slide" data-slider="#slider-card-why" data-action="slickPrev">
            <img src="<?= get_bloginfo('template_url') . '/img/templates/slider-arrow.svg'; ?>">
        </button>

        <button class="slide-nav-mob slide-nav-mob--next js-change-slide" data-slider="#slider-card-why" data-action="slickNext">
            <img src="<?= get_bloginfo('template_url') . '/img/templates/slider-arrow.svg'; ?>">
        </button>
    </div>
</section>

<section class="c-section-form">
    <img src="<?php bloginfo('template_url'); ?>/img/form-bg-pattern.svg" class="c-section-form__bg" />

    <?php get_template_part('partials/form-horizontal', 'reservation'); ?>
</section>


<?php get_footer(); ?>

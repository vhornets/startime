<?php
/**
 * Template Name: Simply the best
 * Template Post Type: project
 */
?>

<?php get_header(); ?>

<section class="c-section-hero">
    <div class="c-slider-hero__images">
        <img src="<?php echo get_field('teleprojects-hero__image')['url']; ?>" alt="" class="c-slider-hero__image c-slider-hero__image--wa">
    </div>

    <div class="l-container">
        <div class="c-slider-hero">
            <div class="c-slider-hero__slide">
                <div class="o-badge c-slider-hero__badge">
                    <?php the_field('teleprojects-hero__badge'); ?>
                </div>

                <h1 class="c-slider-hero__title">
                    <?php the_field('teleprojects-hero__title'); ?>
                </h1>

                <h3 class="c-slider-hero__sub-title">
                    <?php the_field('teleprojects-hero__subtitle'); ?>
                </h3>
            </div>
        </div>
    </div>
</section>

<section class="c-section-about-course">
    <img src="<?php bloginfo('template_url'); ?>/img/templates/teleprojects/about_bg.svg" class="c-section-about-course__bg" />

    <div class="l-about-course">
        <div class="c-about-camp">
            <h2 class="c-about-camp__title">
                <?php the_field('about-project__title'); ?>
            </h2>

            <div class="c-about-camp__text">
                <?php echo apply_filters('the_content', get_field('about-project__text')); ?>
            </div>

        </div>
    </div>
</section>

<section class="c-section-teachers">
    <?php if(get_field('nominees__display') != 'hide'): ?>

    <img src="<?php bloginfo('template_url'); ?>/img/teachers-bg-pattern.svg" class="c-section-teachers__bg" />

    <div class="l-container">
        <h2 class="c-slider-teachers__title">
            <?php the_field('nominees__title'); ?>
        </h2>

        <div class="c-slider-teachers__controls slick-arrows">
            <button class="slick-arrow c-slider-teachers__arrow c-slider-teachers__arrow--prev js-change-slide" data-slider="#slider-teachers" data-action="slickPrev">
                <i class="icon svg-slider-arrow-left svg-slider-arrow-left-dims"></i>
            </button>

            <button class="slick-arrow c-slider-teachers__arrow c-slider-teachers__arrow--next js-change-slide" data-slider="#slider-teachers" data-action="slickNext">
                <i class="icon svg-slider-arrow-right svg-slider-arrow-right-dims"></i>
            </button>
        </div>
    </div>

    <?php $nominees = get_posts([
        'post_type' => 'nominee',
        'posts_per_page' => -1
    ]); ?>

    <div class="c-slider-teachers nominees" id="slider-teachers">
        <?php foreach($nominees as $post): setup_postdata($post); ?>
            <div class="c-slider-teachers__slide">
                <div class="c-card-person c-card-person--artist">
                    <div class="c-card-person__content">
                        <div class="c-card-person__image">
                            <img src="<?php echo get_thumbnail_src('medium'); ?>" title="<?php the_title(); ?>" alt="<?php the_title(); ?>">
                        </div>

                        <div class="c-card-person__name">
                            <?php $title = explode(' ', get_the_title()); ?>

                            <span><?php echo $title[0]; ?></span> <?php echo $title[1]; ?>
                        </div>

                        <div class="c-card-person__age">
                            <?php the_field('age'); ?>, <?php the_field('occupation'); ?>
                        </div>

                        <div class="c-card-person__description-short">
                            <?php the_field('decription'); ?>
                        </div>

                        <div class="c-card-person__button-block">
                            <div class="c-card-person__votes-data">
                                <span class="c-card-person__votes-value"><?php the_field('votes'); ?></span>
                                <span><?php _e('голоса', 'startime'); ?></span>
                            </div>

                            <form class="js-nominee-vote-form">
                                <button type="submit" class="o-button-default c-card__button <?php if(isset($_COOKIE["nomineeVote"])): ?>is-disabled<?php endif; ?>" data-submitting-text="<?php _e('Отправка', 'startime'); ?>..." data-voted-text="<?php _e('Голос учтен', 'startime'); ?>">
                                    <?php if(isset($_COOKIE["nomineeVote"]) && htmlspecialchars($_COOKIE["nomineeVote"]) == get_the_ID()): ?>
                                        <?php _e('Голос учтен', 'startime'); ?>
                                    <?php else: ?>
                                        <?php _e('Голосовать', 'startime'); ?>
                                    <?php endif; ?>
                                </button>

                                <?php wp_nonce_field('send_vote'); ?>
                                <input type="hidden" name="nominee-id" value="<?php the_ID(); ?>">
                                <input type="hidden" name="action" value="sendVote">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; wp_reset_postdata(); ?>
    </div>

    <?php endif; ?>
</section>

<section class="c-section-translation">
    <div class="l-container">
        <h2 class="c-slider-teachers__title"><?php the_field('broadcasts__title'); ?></h2>

        <div class="c-slider-teachers__controls slick-arrows">
            <button class="slick-arrow c-slider-teachers__arrow c-slider-teachers__arrow--prev js-change-slide" data-slider="#slider-translation" data-action="slickPrev">
                <i class="icon svg-slider-arrow-left svg-slider-arrow-left-dims"></i>
            </button>

            <button class="slick-arrow c-slider-teachers__arrow c-slider-teachers__arrow--next js-change-slide" data-slider="#slider-translation" data-action="slickNext">
                <i class="icon svg-slider-arrow-right svg-slider-arrow-right-dims"></i>
            </button>
        </div>
    </div>

    <div class="l-container">
        <div class="c-slider-translation" id="slider-translation">
            <?php foreach(get_field('logos') as $logo): ?>
                <div class="c-slider-translation__slide">
                    <div class="c-card-translation">
                        <img src="<?php echo $logo['sizes']['medium']; ?>" title="Эстрадная школа" alt="Эстрадная школа">
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>

<section class="c-section-nominees-block">
    <img src="<?php bloginfo('template_url'); ?>/img/templates/teleprojects/nom-block-bg.svg" class="nominees-block__bg" />

    <div class="l-container">
        <h2 class="text-center">
            <?php the_field('nominations__title'); ?>
        </h2>
    </div>

    <div class="l-container">
        <?php foreach(get_field('nominations') as $num => $nomination): ?>
            <?php list($r, $g, $b) = sscanf($nomination['color-to'], "#%02x%02x%02x"); ?>

            <style type="text/css">
                #nomination-<?php echo $num; ?> .nominees-block__data {
                    background: linear-gradient(241.22deg, <?php echo $nomination['color-from']; ?> 12.19%, <?php echo $nomination['color-to']; ?> 83.59%);
                    box-shadow: 0px 30px 50px rgba(<?php echo $r; ?>, <?php echo $g; ?>, <?php echo $b; ?>, 0.3);
                }

                #nomination-<?php echo $num; ?> .c-card-lesson__image svg path {
                    fill: url(#nomination-<?php echo $num; ?>-gradient) <?php echo $nomination['color-from']; ?>;
                }

                #nomination-<?php echo $num; ?> .c-card-lesson__image {
                    position: static;
                    padding-top: 45px;
                }

                #nomination-<?php echo $num; ?> .text-color {
                    color: <?php echo $nomination['color-to']; ?>;
                }
            </style>

            <div class="nominees-block__single" id="nomination-<?php echo $num; ?>">
                <div class="nominees-block__header">
                    <div class="c-card-lesson__image">
<!--                        <svg style="width:0;height:0;position:absolute;" aria-hidden="true" focusable="false">-->
<!--                            <linearGradient id="nomination---><?php //echo $num; ?><!---gradient" x2="1" y2="1">-->
<!--                                <stop offset="12%" stop-color="--><?php //echo $nomination['color-from']; ?><!--" />-->
<!--                                <stop offset="83.59%" stop-color="--><?php //echo $nomination['color-to']; ?><!--" />-->
<!--                            </linearGradient>-->
<!--                        </svg>-->

<!--                        --><?php //echo file_get_contents( get_bloginfo('template_url') . '/img/lesson-card-pattern.svg' ); ?>

                        <img src="<?php echo $nomination['image']['url']; ?>" alt="" />
                    </div>
                </div>

                <div class="nominees-block__data">
                    <h4><?php echo $nomination['title']; ?></h4>

                    <div class="description"><?php echo $nomination['text']; ?></div>

                    <div class="ages">
                        <?php foreach($nomination['list'] as $listItem): ?>
                            <p class="text-color"><?php echo $listItem['text']; ?></p>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</section>

<section class="c-section-photos">
    <?php get_template_part('partials/gallery'); ?>
</section>

<section class="c-section-video">
    <?php get_template_part('partials/video'); ?>
</section>

<section class="c-section-form">
    <img src="<?php bloginfo('template_url'); ?>/img/form-bg-pattern.svg" class="c-section-form__bg" />

    <?php get_template_part('partials/form-horizontal'); ?>
</section>

<?php get_footer(); ?>

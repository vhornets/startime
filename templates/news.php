<?php
/**
 * Template Name: News
 */
?>

<?php
$tabs = [
    [
        'key' => 'all',
        'value' => 'Все'
    ],
    [
        'key' => 2019,
        'value' => 2019
    ],
    [
        'key' => 2018,
        'value' => 2018
    ]
]
?>

<?php get_header(); ?>

<section class="c-section-main-bg js-tabs">
    <img src="<?php bloginfo('template_url'); ?>/img/templates/news/news_bg.svg" class="camp_main__img" />
    <img src="<?php bloginfo('template_url'); ?>/img/templates/camp/camp_main_photo_bg.svg" class="camp_main__bg" />

    <div class="l-position text-center">
        <div class="l-container text-color">
            <h2 class="c-section-main-bg__title">
                Наши новости
            </h2>
            <ul class="c-tabs-nav js-tabs-nav">
                <?php foreach($tabs as $tab): ?>
                    <li>
                        <a href="#tab-<?= $tab['key']; ?>" class="js-toggle-tab <?php if($tab['key'] == 'all' ): ?>is-init<?php endif; ?>">
                            <?= $tab['value']; ?>
                        </a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
</section>

<section class="c-section-news js-tabs">
    <div class="l-container">
        <div class="news-block">
            <?php foreach ($tabs as $tab): ?>
                <div class="js-tab" id="tab-<?= $tab['key']; ?>">
                    <div class="news-block__data">
                        <?php for ($i = 0; $i < 4; $i++): ?>
                            <a href="#" class="c-card c-card--has-footer">
                                <div class="c-card__image" style="background-image: url('<?php bloginfo('template_url'); ?>/img/templates/recording-songs-2/news-image-4.jpg');"></div>

                                <div class="c-card__body">
                                    <div class="c-card__title">
                                        КАСТИНГ на роль Гарри Поттера
                                    </div>

                                    <div class="c-card__text">
                                        Не упусти возможность стать главным героем истории о волшебниках.

                                        За подробной информацией о кастинге по телефону:
                                        +38(093) 579-99-99
                                    </div>

                                    <ul class="c-card__footer">
                                        <li>
                                            <div class="c-card__date">
                                                07.08.<?= $tab['key'] === 'all' ? 2020 : $tab['key'] ?>
                                            </div>
                                        </li>

                                        <li>
                                            <div class="c-card__more">
                                                <?php _e('подробнее', 'startime'); ?>
                                                <?php echo file_get_contents( get_bloginfo('template_url') . '/assets/svg/slider-arrow-right.svg' ); ?>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </a>
                            <a href="#" class="c-card c-card--has-footer">
                                <div class="c-card__image" style="background-image: url('<?php bloginfo('template_url'); ?>/img/templates/recording-songs-2/news-image-5.jpg');"></div>

                                <div class="c-card__body">
                                    <div class="c-card__title">
                                        Новый выпуск шоу для девочек
                                    </div>

                                    <div class="c-card__text">
                                        Суббота — это день, когда никуда не нужно спешить!🥰

                                        Хорошенько поваляйтесь в кровати за просмотром веселого, крутого, современного шоу
                                    </div>

                                    <ul class="c-card__footer">
                                        <li>
                                            <div class="c-card__date">
                                                06.07.<?= $tab['key'] === 'all' ? 2020 : $tab['key'] ?>
                                            </div>
                                        </li>

                                        <li>
                                            <div class="c-card__more">
                                                <?php _e('подробнее', 'startime'); ?>
                                                <?php echo file_get_contents( get_bloginfo('template_url') . '/assets/svg/slider-arrow-right.svg' ); ?>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </a>
                        <?php endfor; ?>
                    </div>
                    <button class="o-button-default c-slider-hero__button">
                        Показать еше
                    </button>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>


<?php get_footer(); ?>

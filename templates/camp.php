<?php
/**
 * Template Name: Camp
 */
?>

<?php get_header(); ?>

<section class="c-section-main-bg">
    <img src="<?php echo get_field('camp-head__image')['url']; ?>" class="camp_main__img" />
    <img src="<?php bloginfo('template_url'); ?>/img/templates/camp/camp_main_photo_bg.svg" class="camp_main__bg" />

    <div class="l-position">
        <div class="l-container">
            <h2 class="c-section-main-bg__title">
                <?php the_field('camp-head__title'); ?>
            </h2>

            <div class="c-section-main-bg__form">
                <div class="left-block">
                    <?php foreach(get_field('camp-head__list') as $item): ?>
                        <div class="c-section-main-bg__single">
                            <?php echo $item['title']; ?>

                            <span><?php echo $item['text']; ?></span>
                        </div>
                    <?php endforeach; ?>
                </div>

                <div class="right-block">
                    <div class="c-section-main-bg__email">
                        <a href="<?php the_field('camp-head__link-url'); ?>">
                            <?php the_field('camp-head__link-text'); ?>
                        </a>
                    </div>

                    <div class="c-section-main-bg__button">
                        <a href="<?php the_field('camp-head__button-href'); ?>" class="o-button-default open-popup" data-open="booking" data-section="<?php echo strip_tags(get_field('camp-head__title')); ?>">
                            <?php the_field('camp-head__button-text'); ?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="c-section-about-course">
    <img src="<?php bloginfo('template_url'); ?>/img/templates/camp/camp_about_bg_1.svg" class="c-section-about-course__bg" />
    <img src="<?php bloginfo('template_url'); ?>/img/templates/camp/camp_about_bg_2.svg" class="c-section-about-course__decor" />

    <div class="l-about-course">
        <div class="c-about-camp">
            <h2 class="c-about-camp__title">
                <?php the_field('about-camp__title'); ?>
            </h2>

            <h5 class="c-about-camp__title2">
                <?php the_field('about-camp__subtitle'); ?>
            </h5>

            <div class="c-about-camp__text">
                <?php echo apply_filters('the_content', get_field('about-camp__text')); ?>
            </div>

            <ul class="c-list-icons">
                <?php foreach(get_field('about-camp__list') as $item): ?>
                    <li>
                        <div class="c-list-icons__icon">
                            <a href="<?php echo $item['link']['url']; ?>">
                                <img src="<?php echo $item['icon']['url']; ?>" title="<?php echo $item['text']; ?>" alt="<?php echo $item['text']; ?>"/>
                            </a>
                        </div>

                        <div class="c-list-icons__title">
                            <a href="<?php echo $item['link']['url']; ?>" target="_blank">
                                <?php echo $item['text']; ?>
                            </a>
                        </div>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
</section>

<section class="icon-tabs js-tabs">
    <div class="l-container">
        <h2 class="text-center">
            <?php the_field('field_shifts_title'); ?>
        </h2>
    </div>

    <div class="l-container">
        <ul class="icon-tabs__header js-tabs-nav hidden--mobile">
            <?php foreach(get_field('shifts') as $num => $shift): ?>
                <li>
                    <a href="#shift-tab-<?php echo $num; ?>" class="js-toggle-tab <?php if($num == 0): ?>is-init<?php endif; ?>">
                        <img src="<?php echo $shift['icon']['url']; ?>" title="<?php echo $shift['name']; ?>" alt="<?php echo $shift['name']; ?>"/>
                        <span><?php echo $shift['name']; ?></span>
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>

        <div class="js-tabs-nav visible--mobile">
            <div class="icon-tabs__header c-slider-blocks-mobile" id="slider-icon-tabs">
                <?php foreach(get_field('shifts') as $num => $shift): ?>
                    <div class="icon-tab-el">
                        <a href="#shift-tab-<?php echo $num; ?>" class="js-toggle-tab <?php if($num == 0): ?>is-init<?php endif; ?>">
                            <img src="<?php echo $shift['icon']['url']; ?>" title="<?php echo $shift['name']; ?>" alt="<?php echo $shift['name']; ?>"/>
                            <span><?php echo $shift['name']; ?></span>
                        </a>
                    </div>
                <?php endforeach; ?>
            </div>
            <div class="slide-nav-arrows">
                <button class="slide-nav-mob slide-nav-mob--prev js-change-slide" data-slider="#slider-icon-tabs" data-action="slickPrev">
                    <?php echo file_get_contents( get_bloginfo('template_url') . '/img/templates/slider-arrow.svg' ); ?>
                </button>

                <button class="slide-nav-mob slide-nav-mob--next js-change-slide" data-slider="#slider-icon-tabs" data-action="slickNext">
                    <?php echo file_get_contents( get_bloginfo('template_url') . '/img/templates/slider-arrow.svg' ); ?>
                </button>
            </div>
        </div>

        <div class="icon-tabs__data">
            <?php foreach(get_field('shifts') as $num => $shift): ?>
                <div class="js-tab js-camp-shift-tab" id="shift-tab-<?php echo $num; ?>">
                    <div class="icon-tabs__data-container">
                        <div class="icon-tabs__data-title">
                            <h6>CМЕНА «<?php echo $shift['name']; ?>»</h6>
                            <p><?php echo $shift['dates']; ?></p>
                            <a href="<?php echo $shift['link']; ?>"><?php echo $shift['link']; ?></a>
                        </div>

                        <div class="icon-tabs__data-description">
                            <?php echo $shift['text']; ?>
                        </div>

                        <div class="icon-tabs__data-price">
                            <div><span><?php echo $shift['price-striken']; ?></span><?php echo $shift['price']; ?></div>
                            <p><?php echo $shift['footnote']; ?></p>
                        </div>

                        <div class="icon-tabs__data-button">
                            <button class="o-button-default open-popup" data-open="booking" data-section="<?php echo strip_tags(get_field('field_shifts_title')); ?>: CМЕНА «<?php echo strip_tags($shift['name']); ?>»">
                                <?php _e('Забронировать место', 'startime'); ?>
                            </button>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>

<section class="schedule-section">
    <img src="<?php bloginfo('template_url'); ?>/img/templates/camp/shudle_bg.svg" class="schedule__bg" />

    <?php foreach(get_field('shifts') as $num => $shift): ?>
        <div class="schedule__shift" style="<?php if($num != 0): ?>display: none;<?php endif; ?>" id="shift-schedule-<?php echo $num; ?>">
            <h2 class="text-center">
                <?php echo $shift['schedule__title']; ?>
                <p>CМЕНА «<?php echo $shift['name']; ?>»</p>
            </h2>

            <?php if($shift['schedule']): ?>
                <div class="schedule__container hidden--mobile">
                    <?php foreach($shift['schedule'] as $card): ?>
                        <div class="schedule__data">
                            <img src="<?php echo $card['image']['sizes']['medium_large'] ?>" title="<?php echo $card['title']; ?>" alt="<?php echo $card['title']; ?>">

                            <h6 class="text-center">
                                <?php echo $card['title'] ?>
                            </h6>

                            <p><?php echo $card['text'] ?></p>
                        </div>
                    <?php endforeach; ?>
                </div>

                <div class="schedule__container c-slider-news visible--mobile" id="slider-schedule-<?php echo $num; ?>">
                    <?php foreach($shift['schedule'] as $card): ?>
                        <div class="schedule__data">
                            <img src="<?php echo $card['image']['sizes']['medium_large'] ?>" title="<?php echo $card['title']; ?>" alt="<?php echo $card['title']; ?>">

                            <h6 class="text-center">
                                <?php echo $card['title'] ?>
                            </h6>

                            <p><?php echo $card['text'] ?></p>
                        </div>
                    <?php endforeach; ?>
                </div>

                <div class="slide-nav-arrows visible--mobile">
                    <button class="slide-nav-mob slide-nav-mob--prev js-change-slide" data-slider="#slider-schedule-<?php echo $num; ?>" data-action="slickPrev">
                        <?php echo file_get_contents( get_bloginfo('template_url') . '/img/templates/slider-arrow.svg' ); ?>
                    </button>

                    <button class="slide-nav-mob slide-nav-mob--next js-change-slide" data-slider="#slider-schedule-<?php echo $num; ?>" data-action="slickNext">
                        <?php echo file_get_contents( get_bloginfo('template_url') . '/img/templates/slider-arrow.svg' ); ?>
                    </button>
                </div>
            <?php endif; ?>
        </div>
    <?php endforeach; ?>
</section>

<section class="camp-features">
    <img src="<?php bloginfo('template_url'); ?>/img/templates/camp/camp_features_bg.svg" class="camp-features__bg" />

    <div class="l-container">
        <h2 class="text-center">
            <?php the_field('features__title'); ?>
        </h2>
    </div>

    <div class="camp-features__container hidden--mobile">
        <?php foreach(get_field('features') as $card): ?>
            <div class="camp-features__data">
                <img src="<?php echo $card['image']['sizes']['medium_large'] ?>" title="<?php echo $card['title']; ?>" alt="<?php echo $card['title']; ?>"> 

                <?php if($card['list']): ?>
                    <ul>
                        <?php foreach($card['list'] as $item): ?>
                            <li><?php echo $item['text']; ?></li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>

                <?php if($card['title']): ?>
                    <h6 class="text-center">
                        <?php echo $card['title']; ?>
                    </h6>
                <?php endif; ?>

                <?php if($card['text']): ?>
                    <?php echo apply_filters('the_content', $card['text']); ?>
                <?php endif; ?>
            </div>
        <?php endforeach; ?>
    </div>

    <div class="camp-features__container c-slider-news visible--mobile" id="slider-camp-features">
        <?php foreach(get_field('features') as $card): ?>
            <div class="camp-features__data">
                <img src="<?php echo $card['image']['sizes']['medium_large'] ?>" title="<?php echo $card['title']; ?>" alt="<?php echo $card['title']; ?>">

                <?php if($card['list']): ?>
                    <ul>
                        <?php foreach($card['list'] as $item): ?>
                            <li><?php echo $item['text']; ?></li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>

                <?php if($card['title']): ?>
                    <h6 class="text-center">
                        <?php echo $card['title']; ?>
                    </h6>
                <?php endif; ?>

                <?php if($card['text']): ?>
                    <?php echo apply_filters('the_content', $card['text']); ?>
                <?php endif; ?>
            </div>
        <?php endforeach; ?>
    </div>
    <div class="slide-nav-arrows visible--mobile">
        <button class="slide-nav-mob slide-nav-mob--prev js-change-slide" data-slider="#slider-camp-features" data-action="slickPrev">
            <?php echo file_get_contents( get_bloginfo('template_url') . '/img/templates/slider-arrow.svg' ); ?>
        </button>

        <button class="slide-nav-mob slide-nav-mob--next js-change-slide" data-slider="#slider-camp-features" data-action="slickNext">
            <?php echo file_get_contents( get_bloginfo('template_url') . '/img/templates/slider-arrow.svg' ); ?>
        </button>
    </div>
</section>

<section class="our-conditions js-tabs">
    <img src="<?php bloginfo('template_url'); ?>/img/templates/camp/our_conditions_bg.svg" class="our-conditions__bg" />

    <div class="l-container">
        <h2 class="text-center">
            <?php the_field('conditions__title'); ?>
        </h2>
    </div>

    <div class="l-container">
        <ul class="our-conditions__header js-tabs-nav">
            <?php foreach(get_field('conditions__tabs') as $num => $tab): ?>
                <li>
                    <a href="#tab-conditions-<?php echo $num; ?>" class="js-toggle-tab <?php if($num == 0): ?>is-init<?php endif; ?>">
                        <img src="<?php echo $tab['icon']['url']; ?>" title="<?php echo $tab['name']; ?>" alt="<?php echo $tab['name']; ?>" />
                        <span><?php echo $tab['name']; ?></span>
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>

        <div class="our-conditions__data">
            <?php foreach(get_field('conditions__tabs') as $num => $tab): ?>
                <div class="js-tab" id="tab-conditions-<?php echo $num; ?>">
                    <div class="our-conditions__data-container">
                        <div class="our-conditions__data-container_image">
                            <img src="<?php echo $tab['image']['sizes']['medium_large'] ?>" title="<?php echo $tab['name']; ?>" alt="<?php echo $tab['name']; ?>" />
                        </div>

                        <div class="our-conditions__data-container_description">
                            <?php if($tab['template'] == 'accomodation'): ?>
                                <div class="data-container__columns">
                                    <div class="data-container__column">
                                        <h6><?php echo $tab['list2-title']; ?></h6>

                                        <ul>
                                            <?php foreach($tab['list2'] as $item): ?>
                                                <li><?php echo $item['text']; ?></li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </div>

                                    <div class="data-container__column">
                                        <h6><?php echo $tab['list3-title']; ?></h6>
                                        <ul>
                                            <?php foreach($tab['list3'] as $item): ?>
                                                <li><?php echo $item['text']; ?></li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tooltip">

                                </div>
                            <?php endif; ?>

                            <?php if($tab['template'] == 'food'): ?>
                                <p><?php echo $tab['text']; ?></p>

                                <ul>
                                    <?php foreach($tab['list'] as $item): ?>
                                        <li><?php echo $item['text']; ?></li>
                                    <?php endforeach; ?>
                                </ul>
                            <?php endif; ?>

                            <?php if($tab['template'] == 'leisure'): ?>
                                <ul>
                                    <?php foreach($tab['list'] as $item): ?>
                                        <li><?php echo $item['text']; ?></li>
                                    <?php endforeach; ?>
                                </ul>
                            <?php endif; ?>
                        </div>
                    </div>

                    <?php if($tab['template'] == 'food'): ?>
                        <div class="our-conditions__button">
                            <a href="<?php echo get_field('conditions__button-href')['url']; ?>" class="o-button-default" target="_blank">
                                <img src="<?php bloginfo('template_url'); ?>/img/templates/camp/cloud.svg" />
                                <span><?php the_field('conditions__button-text'); ?></span>
                            </a>
                        </div>
                    <?php endif; ?>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>

<section class="c-section-form">
    <img src="<?php bloginfo('template_url'); ?>/img/form-bg-pattern.svg" class="c-section-form__bg" />

    <?php get_template_part('partials/form-horizontal'); ?>
</section>

<section class="c-section-photos">
    <?php get_template_part('partials/gallery'); ?>
</section>

<section class="c-section-teachers js-tabs">
    <?php if(get_field('crew__display') != 'hide'): ?>
        <?php get_template_part('partials/crew'); ?>
    <?php endif; ?>
</section>

<section class="c-section-video hidden--mobile">
    <?php get_template_part('partials/video'); ?>
</section>

<section class="c-section-form">
    <img src="<?php bloginfo('template_url'); ?>/img/form-bg-pattern.svg" class="c-section-form__bg" />

    <?php get_template_part('partials/form-horizontal'); ?>
</section>

<?php get_footer(); ?>

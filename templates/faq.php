<?php
/**
 * Template Name: FAQ
 */
?>

<?php get_header(); ?>

<section class="c-section-main-bg">
    <img src="<?php echo get_field('faq-head__image')['url']; ?>" class="camp_main__img" />
    <img src="<?php bloginfo('template_url'); ?>/img/templates/news/news_bg_top.png" class="camp_main__bg" />

    <div class="l-position text-center">
        <div class="l-container text-color">
            <h2 class="c-section-main-bg__title">
               <?php the_field('faq-head__title'); ?>
            </h2>

            <button class="o-button-default o-button-default--purple open-popup" data-open="entry" data-section="<?php echo strip_tags(get_field('faq-head__title')); ?>">
                <?php the_field('faq-head__button-text'); ?>
            </button>
        </div>
    </div>
</section>

<section class="c-section-faq js-accordion">
    <img src="<?php bloginfo('template_url'); ?>/img/templates/producing/producing_bg.svg" class="c-section-producing__bg" />

    <div class="c-section-faq__container">
        <?php foreach(get_field('faq__questions') as $faq): ?>
            <article class="c-section-faq__single js-accordion-tab">
                <div class="c-section-faq__single--header">
                    <h6><?php echo $faq['question']; ?></h6>

                    <div class="icon">
                        <img src="<?php bloginfo('template_url'); ?>/img/templates/faq/arrow.svg">
                    </div>
                </div>

                <div class="c-section-faq__single--data">
                    <p class="bold"><?php echo $faq['list-title']; ?></p>

                    <ul>
                        <?php foreach($faq['list'] as $listItem): ?>
                            <li><?php echo $listItem['text']; ?></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </article>
        <?php endforeach; ?>
    </div>
</section>

<section class="c-section-form">
    <img src="<?php bloginfo('template_url'); ?>/img/form-bg-pattern.svg" class="c-section-form__bg" />

    <?php get_template_part('partials/form-horizontal'); ?>
</section>


<?php get_footer(); ?>

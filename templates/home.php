<?php
/**
* Template Name: Главная
*/
?>

<?php get_header(); ?>

<?php if(have_posts()): the_post(); ?>

<section class="c-section-hero">
    <div class="c-slider-hero__images">
        <?php foreach(get_field('hero__slides') as $slide): ?>
            <img src="<?php echo $slide['image']['url']; ?>" alt="Эстрадная школа" title="Эстрадная школа - Startime" class="c-slider-hero__image">
        <?php endforeach; ?>

        <img src="<?php bloginfo('template_url'); ?>/img/decor1.svg" alt="" class="c-slider-hero__decor c-slider-hero__decor--1">
        <img src="<?php bloginfo('template_url'); ?>/img/decor2.svg" alt="" class="c-slider-hero__decor c-slider-hero__decor--2">
        <img src="<?php bloginfo('template_url'); ?>/img/decor3.svg" alt="" class="c-slider-hero__decor c-slider-hero__decor--3">
        <img src="<?php bloginfo('template_url'); ?>/img/decor4.svg" alt="" class="c-slider-hero__decor c-slider-hero__decor--4">
    </div>

    <div class="l-container">
        <div class="c-slider-hero">
            <?php foreach(get_field('hero__slides') as $slide): ?>
                <div class="c-slider-hero__slide">
                    <h1 class="c-slider-hero__title">
                        <?php echo $slide['title']; ?>
                    </h1>

                    <?php if($slide['countdown']): ?>
                        <div class="c-countdown c-slider-hero__countdown" data-date="<?php echo $slide['countdown']; ?>">
                            <div class="c-countdown__digit">
                                <span class="c-countdown__value c-countdown__days">17</span>
                                <span class="c-countdown__text">Дней</span>
                            </div>

                            <div class="c-countdown__separator">:</div>

                            <div class="c-countdown__digit">
                                <span class="c-countdown__value c-countdown__hours">09</span>
                                <span class="c-countdown__text">Часов</span>
                            </div>

                            <div class="c-countdown__separator">:</div>

                            <div class="c-countdown__digit">
                                <span class="c-countdown__value c-countdown__minutes">45</span>
                                <span class="c-countdown__text">Минут</span>
                            </div>

                            <div class="c-countdown__separator">:</div>

                            <div class="c-countdown__digit">
                                <span class="c-countdown__value c-countdown__seconds">20</span>
                                <span class="c-countdown__text">Секунд</span>
                            </div>
                        </div>
                    <?php endif; ?>

                    <div>
                        <a href="<?php echo $slide['button-href']; ?>" class="o-button-default c-slider-hero__button open-popup" data-open="entry" data-section="<?php echo strip_tags($slide['title']); ?>">
                            <?php echo $slide['button-text']; ?>
                        </a>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>

<section class="c-section-lessons js-tabs" id="courses">
    <img src="<?php bloginfo('template_url'); ?>/img/lessons-bg-pattern.svg" class="c-section-lessons__bg" />

    <div class="l-container text-center">
        <h2><?php the_field('lessons__title'); ?></h2>

        <?php $courseTypes = get_terms([
            'taxonomy' => 'course-type',
            'hide_empty' => false
        ]); ?>

        <ul class="c-tabs-nav js-tabs-nav">
            <?php foreach($courseTypes as $num => $courseType): ?>
                <li>
                    <a href="#lessons-<?php echo $courseType->term_id; ?>" class="js-toggle-tab <?php if($num == 0 ): ?>is-init<?php endif; ?>">
                        <?php echo $courseType->name; ?>
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>

    <?php foreach($courseTypes as $num => $courseType): ?>
        <?php $courses = get_posts([
            'post_type' => 'course',
            'posts_per_page' => -1,
            'tax_query' => [
                [
                    'taxonomy' => 'course-type',
                    'field'    => 'slug',
                    'terms'    => $courseType->slug,
                ],
            ],
        ]); ?>

        <div class="js-tab" id="lessons-<?php echo $courseType->term_id; ?>">
            <div class="l-four-columns hidden--mobile">
                <?php foreach($courses as $post): setup_postdata($post); ?>
                    <?php list($r, $g, $b) = sscanf(get_field('color-from'), "#%02x%02x%02x"); ?>

                    <div class="l-four-columns__item">

                        <a href="<?php the_permalink(); ?>" class="c-card-lesson" id="card-lesson<?php echo $courseType->term_id; ?>-<?php the_ID(); ?>">
                            <div class="c-card-lesson__bg"></div>

                            <div class="c-card-lesson__image">
                                <img src="<?php echo get_thumbnail_src('medium'); ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>" />
                            </div>

                            <div class="c-card-lesson__title">
                                <?php the_title(); ?>
                            </div>

                            <div class="o-button-default c-card-lesson__button">
                                <span><?php _e('Узнать больше', 'startime'); ?></span>
                            </div>
                        </a>
                    </div>
                <?php endforeach; wp_reset_postdata(); ?>
            </div>
            <div class="c-slider-news visible--mobile" id="c-slider-lessons">
                <?php foreach($courses as $post): setup_postdata($post); ?>
                    <?php list($r, $g, $b) = sscanf(get_field('color-from'), "#%02x%02x%02x"); ?>

                    <div class="l-four-columns__item">

                        <a href="<?php the_permalink(); ?>" class="c-card-lesson" id="card-lesson<?php echo $courseType->term_id; ?>-<?php the_ID(); ?>">
                            <div class="c-card-lesson__bg"></div>

                            <div class="c-card-lesson__image">
                                <img src="<?php echo get_thumbnail_src('medium'); ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>" />
                            </div>

                            <div class="c-card-lesson__title">
                                <?php the_title(); ?>
                            </div>

                            <div class="o-button-default c-card-lesson__button">
                                <span><?php _e('Узнать больше', 'startime'); ?></span>
                            </div>
                        </a>
                    </div>
                <?php endforeach; wp_reset_postdata(); ?>
            </div>
            <div class="slide-nav-arrows visible--mobile">
                <button class="slide-nav-mob slide-nav-mob--prev js-change-slide" data-slider="#c-slider-lessons" data-action="slickPrev">
                    <?php echo file_get_contents( get_bloginfo('template_url') . '/img/templates/slider-arrow.svg' ); ?>
                </button>

                <button class="slide-nav-mob slide-nav-mob--next js-change-slide" data-slider="#c-slider-lessons" data-action="slickNext">
                    <?php echo file_get_contents( get_bloginfo('template_url') . '/img/templates/slider-arrow.svg' ); ?>
                </button>
            </div>
        </div>
    <?php endforeach; ?>
</section>

<section class="c-section-teachers js-tabs">
    <?php get_template_part('partials/crew'); ?>
</section>

<section class="c-section-about">
    <div class="l-about">
        <div class="l-about__image">
            <div class="c-about__image" style="background-image: url('<?php echo get_field('about__image')['sizes']['medium_large']; ?>');"></div>
        </div>

        <div class="l-about__text">
            <h5 class="c-about__name"><?php the_field('about__title'); ?></h5>

            <div class="c-about__description">
                <?php echo apply_filters('the_content', get_field('about__text')); ?>
            </div>

            <h3 class="c-about__title"><?php the_field('about__title2'); ?></h3>

            <div class="c-about__text">
                <?php echo apply_filters('the_content', get_field('about__text2')); ?>
            </div>

            <a href="<?php the_field('about__button-href'); ?>" class="o-button-default c-about__button">
                <?php the_field('about__button-text'); ?>
            </a>
        </div>
    </div>
</section>

<section class="c-section-feedback js-tabs">
    <img src="<?php bloginfo('template_url'); ?>/img/feedback-bg-pattern.svg" class="c-section-feedback__bg" />

    <?php $feedbackTypes = get_terms([
        'taxonomy' => 'type',
        'hide_empty' => false
    ]); ?>

    <div class="l-container c-slider-feedback__head">
        <div class="c-slider-feedback__head">
            <h2 class="c-slider-feedback__tabs-title"><?php the_field('feedback__title'); ?></h2>

            <ul class="c-tabs-nav c-slider-feedback__tabs-controls js-tabs-nav">
                <?php foreach($feedbackTypes as $num => $feedbackType): ?>
                    <li>
                        <a href="#feedback-<?php echo $feedbackType->term_id; ?>" class="js-toggle-tab <?php if($num == 0): ?>is-init<?php endif; ?>">
                            <?php echo $feedbackType->name; ?>
                        </a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>

    <div class="l-feedback">
        <?php foreach($feedbackTypes as $num => $feedbackType): ?>
            <?php $feedback = get_posts([
                'post_type' => 'review',
                'posts_per_page' => -1,
                'tax_query' => [
                    [
                        'taxonomy' => 'type',
                        'field'    => 'slug',
                        'terms'    => $feedbackType->slug,
                    ],
                ],
            ]); ?>

            <div class="js-tab" id="feedback-<?php echo $feedbackType->term_id; ?>">
                <div class="c-slider-feedback__photos">
                    <?php $num = 0; foreach($feedback as $post): ?>
                        <?php
                            @$prevPost = $feedback[$num + 1];
                            @$nextPost = $feedback[$num - 1];

                            if(!$prevPost) {
                                $prevPost = $feedback[0];
                            }

                            if(!$nextPost) {
                                $nextPost = $feedback[count($feedback) - 1];
                            }
                        ?>

                        <div class="c-slider-feedback__photos-set">
                            <div class="c-slider-feedback__photo" style="background-image: url('<?php echo get_thumbnail_src('medium_large', $nextPost->ID); ?>');"></div>
                            <div class="c-slider-feedback__photo" style="background-image: url('<?php echo get_thumbnail_src('medium_large', $prevPost->ID); ?>');"></div>
                            <div class="c-slider-feedback__photo" style="background-image: url('<?php echo get_thumbnail_src('medium_large', $post->ID); ?>');"></div>
                        </div>
                    <?php $num++; endforeach; ?>
                </div>

                <div class="c-slider-feedback__wrap">
                    <i class="icon svg-quote svg-quote-dims"></i>

                    <div class="c-slider-feedback" id="slider-feedback-<?php echo $num; ?>">
                        <?php foreach($feedback as $post): ?>
                            <div class="c-slider-feedback__slide">
                                <div class="c-slider-feedback__text">
                                    <?php echo get_the_content(false, false, $post); ?>
                                </div>

                                <div class="c-slider-feedback__title">
                                    <?php echo get_the_title($post); ?>
                                </div>

                                <!-- <div class="c-slider-feedback__description">
                                    <?php //echo $feedback['description']; ?>
                                </div> -->
                            </div>
                        <?php endforeach; ?>
                    </div>

                    <div class="c-slider-feedback__controls slick-arrows">
                        <button class="slick-arrow c-slider-feedback__arrow c-slider-feedback__arrow--prev js-change-slide" data-slider=".c-slider-feedback" data-action="slickPrev">
                            <?php echo file_get_contents( get_bloginfo('template_url') . '/assets/svg/slider-arrow-left.svg' ); ?>
                        </button>

                        <button class="slick-arrow c-slider-feedback__arrow c-slider-feedback__arrow--next js-change-slide" data-slider=".c-slider-feedback" data-action="slickNext">
                            <?php echo file_get_contents( get_bloginfo('template_url') . '/assets/svg/slider-arrow-right.svg' ); ?>
                        </button>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</section>

<section class="c-section-news js-tabs">
    <div class="l-container">
        <ul class="c-tabs-nav-big js-tabs-nav">
            <li>
                <a href="#projects" class="js-toggle-tab is-init">
                    <?php _e('Наши проекты', 'startime'); ?>
                </a>
            </li>

            <li>
                <a href="#news" class="js-toggle-tab">
                    <?php _e('Наши новости', 'startime'); ?>
                </a>
            </li>
        </ul>

        <div class="js-tab" id="news">
            <?php $news = get_posts([
                'posts_per_page' => 8,
                'post_type' => 'post'
            ]); ?>

            <div class="c-slider-news__controls slick-arrows">
                <button class="slick-arrow c-slider-teachers__arrow c-slider-news__arrow--prev js-change-slide" data-slider="#slider-news" data-action="slickPrev">
                    <?php echo file_get_contents( get_bloginfo('template_url') . '/assets/svg/slider-arrow-left.svg' ); ?>
                </button>

                <button class="slick-arrow c-slider-teachers__arrow c-slider-news__arrow--next js-change-slide" data-slider="#slider-news" data-action="slickNext">
                    <?php echo file_get_contents( get_bloginfo('template_url') . '/assets/svg/slider-arrow-right.svg' ); ?>
                </button>
            </div>

            <div class="c-slider-news" id="slider-news">
                <?php foreach($news as $post): setup_postdata($post); ?>
                    <div class="c-slider-news__slide">
                        <?php get_template_part('partials/news-card'); ?>
                    </div>
                <?php endforeach; wp_reset_postdata(); ?>
            </div>

            <div class="slide-nav-arrows visible--mobile">
                <button class="slide-nav-mob slide-nav-mob--prev js-change-slide" data-slider="#slider-news" data-action="slickPrev">
                    <?php echo file_get_contents( get_bloginfo('template_url') . '/img/templates/slider-arrow.svg' ); ?>
                </button>

                <button class="slide-nav-mob slide-nav-mob--next js-change-slide" data-slider="#slider-news" data-action="slickNext">
                    <?php echo file_get_contents( get_bloginfo('template_url') . '/img/templates/slider-arrow.svg' ); ?>
                </button>
            </div>

            <div class="c-slider-news__more">
                <a href="<?php echo get_the_permalink( get_option('page_for_posts') ); ?>" class="o-button-default">
                    Все новости
                </a>
            </div>
        </div>

        <div class="js-tab" id="projects">
            <?php $projects = get_posts([
                'posts_per_page' => -1,
                'post_type' => 'project'
            ]); ?>

            <div class="c-slider-news__controls slick-arrows">
                <button class="slick-arrow c-slider-teachers__arrow c-slider-news__arrow--prev js-change-slide" data-slider="#slider-projects" data-action="slickPrev">
                    <?php echo file_get_contents( get_bloginfo('template_url') . '/assets/svg/slider-arrow-left.svg' ); ?>
                </button>

                <button class="slick-arrow c-slider-teachers__arrow c-slider-news__arrow--next js-change-slide" data-slider="#slider-projects" data-action="slickNext">
                    <?php echo file_get_contents( get_bloginfo('template_url') . '/assets/svg/slider-arrow-right.svg' ); ?>
                </button>
            </div>

            <div class="c-slider-news" id="slider-projects">
                <?php foreach($projects as $post): setup_postdata($post); ?>
                    <div class="c-slider-news__slide">
                        <a href="<?php the_permalink(); ?>" class="c-card">
                            <div class="c-card__image" style="background-image: url('<?php echo get_thumbnail_src('medium_large'); ?>');"></div>

                            <div class="c-card__body">
                                <div class="c-card__title">
                                    <?php the_title(); ?>
                                </div>

                                <div class="c-card__text">
                                    <?php the_content(false); ?>
                                </div>

                                <div class="o-button-default c-card__button">
                                    <?php _e('Читать подробнее', 'startime'); ?>
                                </div>
                            </div>
                        </a>
                    </div>
                <?php endforeach; wp_reset_postdata(); ?>
            </div>

            <div class="slide-nav-arrows visible--mobile">
                <button class="slide-nav-mob slide-nav-mob--prev js-change-slide" data-slider="#slider-projects" data-action="slickPrev">
                    <?php echo file_get_contents( get_bloginfo('template_url') . '/img/templates/slider-arrow.svg' ); ?>
                </button>

                <button class="slide-nav-mob slide-nav-mob--next js-change-slide" data-slider="#slider-projects" data-action="slickNext">
                    <?php echo file_get_contents( get_bloginfo('template_url') . '/img/templates/slider-arrow.svg' ); ?>
                </button>
            </div>
        </div>
    </div>
</section>

<?php endif; ?>
<script type="application/ld+json">{
	"@context" : "http://schema.org",
	"@type" : "Organisation",
	"@id" : "<?=get_home_url(); ?>",
	"image" : "http://startime.devpro.agency/wp-content/themes/startime/img/startime-logo.svg",
	"name" : "Продюсерский центр Star Time",
    "description": "Детский продюсерский центр Стар Тайм - обучение в сфере детского шоу-бизнеса. Творческое развитие детей, съемки в телепроектах и выступления на сцене",
"address" : [{
	"@id" : "<?=get_home_url(); ?>",
	"@type" : "PostalAddress",
	"streetAddress" : "ул. Греческая 1А",
	"addressLocality" : "Одесса",
	"postalCode" : "65026",
	"addressCountry" : "UA",
	"telephone" : "(048) 772-57-87",
    "email" : "info@startime.ua"
}]
}</script>
<?php get_footer(); ?>

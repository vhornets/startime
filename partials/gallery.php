<?php if(get_field('gallery__display') != 'hide' && get_field('gallery')): ?>

<img src="<?php bloginfo('template_url'); ?>/img/photos-bg-pattern.svg" class="c-section-photos__bg" />

<h2 class="text-center">
    <?php the_field('gallery__title'); ?>
</h2>

<div class="c-slider-photos__wrap hidden--mobile">
    <div class="c-slider-photos" id="slider-photos">
        <?php foreach(get_field('gallery') as $image): ?>
            <div class="c-slider-photos__photo" data-slider="#slider-photos" style="background-image: url('<?php echo $image['sizes']['large']; ?>');"></div>
        <?php endforeach; ?>
    </div>

    <button class="slick-arrow c-slider-photos__arrow c-slider-photos__arrow--prev js-change-slide" data-slider="#slider-photos" data-action="slickPrev">
        <i class="icon svg-slider-arrow-left svg-slider-arrow-left-dims"></i>
    </button>

    <button class="slick-arrow c-slider-photos__arrow c-slider-photos__arrow--next js-change-slide" data-slider="#slider-photos" data-action="slickNext">
        <i class="icon svg-slider-arrow-right svg-slider-arrow-right-dims"></i>
    </button>

</div>

    <div class="c-slider-photos__wrap visible--mobile">
        <div class="c-slider-photos" id="slider-photos-mob">
            <?php foreach(get_field('gallery') as $image): ?>
                <div class="c-slider-photos__photo" data-slider="#slider-photos-mob"><img src="<?php echo $image['sizes']['large']; ?>"></div>
            <?php endforeach; ?>
        </div>

        <div class="slide-nav-arrows" style="margin-top: 20px;">
            <button class="slide-nav-mob slide-nav-mob--prev js-change-slide" data-slider="#slider-photos-mob" data-action="slickPrev">
                <?php echo file_get_contents( get_bloginfo('template_url') . '/img/templates/slider-arrow.svg' ); ?>
            </button>

            <button class="slide-nav-mob slide-nav-mob--next js-change-slide" data-slider="#slider-photos-mob" data-action="slickNext">
                <?php echo file_get_contents( get_bloginfo('template_url') . '/img/templates/slider-arrow.svg' ); ?>
            </button>
        </div>
    </div>

<?php endif; ?>

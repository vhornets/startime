<form class="l-form-horizontal js-form-validate js-form-submit" data-popup="horizontal">
    <h3 class="c-form-horizontal__title">
        <?php the_field('form__title'); ?>
    </h3>

    <div class="c-form-horizontal">
        <div class="c-form-horizontal__subtitle">
            <?php the_field('form__form-title'); ?>
        </div>

        <div class="c-form-horizontal__text">
            <?php the_field('form__form-text'); ?>
        </div>

        <div class="l-form-horizontal__row">
            <div class="l-form-horizontal__column">
                <input type="text" name="name" class="c-form__control" placeholder="<?php _e('Ваше имя', 'startime'); ?>" required />
            </div>

            <div class="l-form-horizontal__column">
                <input type="text" name="birthday" class="c-form__control" placeholder="<?php _e('Дата рождения', 'startime'); ?>" required />
            </div>

            <div class="l-form-horizontal__column">
                <input type="tel" name="tel" class="c-form__control" placeholder="<?php _e('Ваш телефон', 'startime'); ?>" required />
            </div>
        </div>

        <button type="submit" class="o-button-default c-form__submit" data-submitting-text="<?php _e('Отправка'); ?>...">
            <?php _e('Отправить', 'startime'); ?>
        </button>

        <?php wp_nonce_field('send_request'); ?>
        <input type="hidden" name="action" value="sendRequest">
        <input type="hidden" name="section" value="">
        <input type="hidden" name="button" value="">
        <!-- <input type="hidden" name="amocrm" value="1"> -->
    </div>
</form>

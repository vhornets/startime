<div class="l-prices__item--single hidden--mobile">
    <?php foreach(get_field('prices') as $num => $item): ?>
        <div class="c-card-price">
            <div class="c-card-price__circle">
                <img src="<?php bloginfo('template_url'); ?>/img/price-circle1.svg" class="c-card-price__circle-outer">

                <div class="c-card-price__circle-inner">
                    <div class="c-card-price__months">
                        <?php echo $item['duration']; ?>
                    </div>
                </div>
            </div>

            <div class="c-card-price__body">
                <div>
                    <div class="c-card-price__title">
                        <?php echo $item['title']; ?>
                    </div>

                    <div class="c-card-price__subtitle">
                        <?php echo $item['subtitle']; ?>
                    </div>
                </div>

                <div>
                    <p class="text-left"><?php echo $item['list-title']; ?></p>

                    <?php if($item['list']): ?>
                        <ul class="c-card-price__list">
                            <?php foreach($item['list'] as $listItem): ?>
                                <li><?php echo $listItem['title']; ?></li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                </div>

                <div>
                    <div class="c-card-price__price">
                        <?php echo $item['price']; ?>
                    </div>

                    <a href="<?php echo $item['button-href']; ?>" class="o-button-default c-card-price__button open-popup" data-open="entry">
                        <?php echo $item['button-text']; ?>
                    </a>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>
<div class="l-prices__item single__mobile visible--mobile">
    <?php foreach(get_field('prices') as $num => $item): ?>
        <div class="c-card-price">
            <div class="c-card-price__circle">
                <img src="<?php bloginfo('template_url'); ?>/img/price-circle1.svg" class="c-card-price__circle-outer">

                <div class="c-card-price__circle-inner">
                    <div class="c-card-price__months">
                        <?php echo $item['duration']; ?>
                    </div>
                </div>
            </div>

            <div class="c-card-price__body">
                <div>
                    <div class="c-card-price__title">
                        <?php echo $item['title']; ?>
                    </div>

                    <div class="c-card-price__subtitle">
                        <?php echo $item['subtitle']; ?>
                    </div>
                </div>

                <div>
                    <p class="text-left"><?php echo $item['list-title']; ?></p>

                    <?php if($item['list']): ?>
                        <ul class="c-card-price__list">
                            <?php foreach($item['list'] as $listItem): ?>
                                <li><?php echo $listItem['title']; ?></li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                </div>

                <div>
                    <div class="c-card-price__price">
                        <?php echo $item['price']; ?>
                    </div>

                    <a href="<?php echo $item['button-href']; ?>" class="o-button-default c-card-price__button open-popup" data-open="entry">
                        <?php echo $item['button-text']; ?>
                    </a>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>

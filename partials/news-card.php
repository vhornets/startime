<a href="<?php the_permalink(); ?>" class="c-card c-card--has-footer">
    <div class="c-card__image" style="background-image: url('<?php echo get_thumbnail_src('medium_large'); ?>');"></div>

    <div class="c-card__body">
        <div class="c-card__title">
            <?php the_title(); ?>
        </div>

        <div class="c-card__text">
            <?php the_field('post__description'); ?>
        </div>

        <ul class="c-card__footer">
            <li>
                <div class="c-card__date">
                    <?php echo get_the_date( 'd.m.y' ); ?>
                </div>
            </li>

            <li>
                <div class="c-card__more">
                    <?php _e('подробнее', 'startime'); ?>
                    <?php echo file_get_contents( get_bloginfo('template_url') . '/assets/svg/slider-arrow-right.svg' ); ?>
                </div>
            </li>
        </ul>
    </div>
</a>
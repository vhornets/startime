<?php if(get_field('videos__display') != 'hide'): ?>

<img src="<?php bloginfo('template_url'); ?>/img/video-bg-pattern.svg" class="c-section-video__bg" />
<img src="<?php bloginfo('template_url'); ?>/img/video-decor.svg" class="c-section-video__decor" />

<h2 class="text-center">
    <?php if(get_field('video__title')): ?>
        <?php the_field('video__title'); ?>
    <?php else: ?>
        <?php _e('Видео', 'startime'); ?>
    <?php endif; ?>
</h2>

<?php $videos = get_field('videos'); ?>

<div class="l-video">
    <div class="l-video__video">
        <div id="video-player" data-plyr-provider="youtube" data-plyr-embed-id="<?php echo getYoutubeVideoId($videos[0]['video']); ?>"></div>
    </div>

    <div class="l-video__playlist">
        <div class="c-video-playlist">
            <?php foreach($videos as $video): ?>
                <div class="c-video" data-embed-id="<?php echo getYoutubeVideoId($video['video']); ?>">
                    <div class="c-video__cover" style="background-image: url('http://img.youtube.com/vi/<?php echo getYoutubeVideoId($video['video']); ?>/mqdefault.jpg');"></div>
                    
                    <div class="c-video__title">
                        <?php echo $video['title']; ?>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>

<?php endif; ?>
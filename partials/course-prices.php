<?php foreach(get_field('prices') as $num => $item): ?>
    <div class="l-prices__item">
        <div class="c-card-price">
            <div class="c-card-price__circle">
                <img src="<?php bloginfo('template_url'); ?>/img/price-circle<?php echo $num; ?>.svg" class="c-card-price__circle-outer">

                <div class="c-card-price__circle-inner">
                    <div class="c-card-price__months">
                        <?php echo $item['duration']; ?>
                    </div>
                </div>
            </div>

            <div class="c-card-price__body">
                <div class="c-card-price__title">
                    <?php echo $item['title']; ?>
                </div>

                <div class="c-card-price__subtitle">
                    <?php echo $item['subtitle']; ?>
                </div>

                <p class="text-left"><?php echo $item['list-title']; ?></p>

                <?php if($item['list']): ?>
                    <ul class="c-card-price__list">
                        <?php foreach($item['list'] as $listItem): ?>
                            <li><?php echo $listItem['title']; ?></li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>

                <div class="c-card-price__price">
                    <?php echo $item['price']; ?>
                </div>

                <div class="c-card-price__footnote">
                    <?php echo $item['footnote']; ?>
                </div>

                <a href="<?php echo $item['button-href']; ?>" class="o-button-default c-card-price__button open-popup" data-open="entry">
                    <?php echo $item['button-text']; ?>
                </a>
            </div>
        </div>
    </div>
<?php endforeach; ?>

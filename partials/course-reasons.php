<div class="l-container">
    <h2 class="text-center">
        <?php the_field('course-reasons__title'); ?>
    </h2>
</div>

<?php
$reasons = get_field('course-reasons');

$reasonsLenght = count($reasons);

$reasonsFirstColumn = array_slice($reasons, 0, $reasonsLenght / 2);
$reasonsSecondColumn = array_slice($reasons, $reasonsLenght / 2);
?>

<div class="l-reasons">
    <div class="l-reasons__column">
        <?php foreach($reasonsFirstColumn as $num => $reason): $num++; ?>
            <div class="c-card-number">
                <div class="c-card-number__circle">
                    <div class="c-card-number__number">
                        <?php echo $num; ?>
                    </div>
                </div>

                <div class="c-card-number__text">
                    <?php echo $reason['title']; ?>
                </div>
            </div>
        <?php endforeach; ?>
    </div>

    <div class="l-reasons__column">
        <?php foreach($reasonsSecondColumn as $reason): $num++; ?>
            <div class="c-card-number">
                <div class="c-card-number__circle">
                    <div class="c-card-number__number">
                        <?php echo $num; ?>
                    </div>
                </div>

                <div class="c-card-number__text">
                    <?php echo $reason['title']; ?>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>

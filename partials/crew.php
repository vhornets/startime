<img src="<?php bloginfo('template_url'); ?>/img/teachers-bg-pattern.svg" class="c-section-teachers__bg" />

<?php $crewCategories = get_terms([
    'taxonomy' => 'crew-category',
    'hide_empty' => false
]); ?>

<div class="l-container">
    <ul class="c-tabs-nav-big js-tabs-nav c-slider-teachers__title">
        <?php foreach($crewCategories as $num => $crewCategory): ?>
            <li>
                <a href="#crew-<?php echo $crewCategory->term_id; ?>" class="js-toggle-tab <?php if($num == 0): ?>is-init<?php endif; ?>">
                    <?php echo $crewCategory->name; ?>
                </a>
            </li>
        <?php endforeach; ?>
    </ul>
</div>

<?php foreach($crewCategories as $num => $crewCategory): ?>
    <div class="js-tab" id="crew-<?php echo $crewCategory->term_id; ?>">
        <div class="l-container">
            <div class="c-slider-teachers__controls slick-arrows">
                <button class="slick-arrow c-slider-teachers__arrow c-slider-teachers__arrow--prev js-change-slide" data-slider="#slider-crew-<?php echo $crewCategory->term_id; ?>" data-action="slickPrev">
                    <i class="icon svg-slider-arrow-left svg-slider-arrow-left-dims"></i>
                </button>

                <button class="slick-arrow c-slider-teachers__arrow c-slider-teachers__arrow--next js-change-slide" data-slider="#slider-crew-<?php echo $crewCategory->term_id; ?>" data-action="slickNext">
                    <i class="icon svg-slider-arrow-right svg-slider-arrow-right-dims"></i>
                </button>
            </div>
        </div>

        <?php $crew = get_posts([
            'post_type' => 'crew',
            'posts_per_page' => -1,
            'tax_query' => [
                [
                    'taxonomy' => 'crew-category',
                    'field'    => 'slug',
                    'terms'    => $crewCategory->slug,
                ],
            ],
        ]); ?>

        <div class="c-slider-teachers" id="slider-crew-<?php echo $crewCategory->term_id; ?>">
            <?php foreach($crew as $post): setup_postdata($post); ?>
                <div class="c-slider-teachers__slide">
                    <?php if($crewCategory->slug == 'artists'): ?>
                        <div class="c-card-person c-card-person--artist">
                            <div class="c-card-person__content">
                                <div class="c-card-person__image" style="background-image: url('<?php echo get_thumbnail_src('medium'); ?>');"></div>

                                <div class="c-card-person__description-short">
                                    <?php the_field('description-short'); ?>
                                </div>

                                <div class="c-card-person__name">
                                    <?php the_title(); ?>
                                </div>
                            </div>
                        </div>
                    <?php else: ?>
                        <?php $title = explode(' ', get_the_title()); ?>

                        <div class="c-card-person">
                            <div class="c-card-person__content">
                                <div class="c-card-person__image" style="background-image: url('<?php echo get_thumbnail_src('medium'); ?>');"></div>

                                <div class="c-card-person__name">
                                    <span><?php echo $title[0]; ?></span> <?php echo $title[1]; ?>
                                </div>

                                <div class="c-card-person__description-short">
                                    <?php the_field('description-short'); ?>
                                </div>

                                <div class="o-button-default c-card-person__button">
                                    <?php _e('Читать подробнее', 'startime'); ?>
                                </div>
                            </div>

                            <div class="c-card-person__description">
                                <div class="c-card-person__name">
                                    <span><?php echo $title[0]; ?></span> <?php echo $title[1]; ?>
                                </div>

                                <div class="c-card-person__description-short">
                                    <?php the_field('description-short'); ?>
                                </div>

                                <div class="c-card-person__description-full">
                                    <?php the_content(); ?>
                                </div>

                                <div class="c-card-person__description-back">
                                    <i class="icon svg-card-arrow-left svg-card-arrow-left-dims"></i>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            <?php endforeach; wp_reset_postdata(); ?>
        </div>

        <div class="slide-nav-arrows visible--mobile">
            <button class="slide-nav-mob slide-nav-mob--prev js-change-slide" data-slider="#slider-crew-<?php echo $crewCategory->term_id; ?>" data-action="slickPrev">
                <?php echo file_get_contents( get_bloginfo('template_url') . '/img/templates/slider-arrow.svg' ); ?>
            </button>

            <button class="slide-nav-mob slide-nav-mob--next js-change-slide" data-slider="#slider-crew-<?php echo $crewCategory->term_id; ?>" data-action="slickNext">
                <?php echo file_get_contents( get_bloginfo('template_url') . '/img/templates/slider-arrow.svg' ); ?>
            </button>
        </div>
    </div>
<?php endforeach; ?>

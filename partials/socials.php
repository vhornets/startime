<li>
    <a href="<?php the_field('instagram_link', 'options'); ?>" target="_blank" rel="nofollow noreferrer">
        <?php echo file_get_contents( get_bloginfo('template_url') . '/assets/svg/instagram.svg' ); ?>
    </a>
</li>

<li>
    <a href="<?php the_field('facebook_link', 'options'); ?>" target="_blank" rel="nofollow noreferrer">
        <?php echo file_get_contents( get_bloginfo('template_url') . '/assets/svg/facebook.svg' ); ?>
    </a>
</li>

<li>
    <a href="<?php the_field('youtube_link', 'options'); ?>" target="_blank" rel="nofollow noreferrer">
        <?php echo file_get_contents( get_bloginfo('template_url') . '/assets/svg/youtube.svg' ); ?>
    </a>
</li>

<div class="l-container">
    <h2 class="c-slider-teachers__title"><?php the_field('crew__title'); ?></h2>

    <div class="c-slider-teachers__controls slick-arrows">
        <button class="slick-arrow c-slider-teachers__arrow c-slider-teachers__arrow--prev js-change-slide" data-slider="#slider-teachers" data-action="slickPrev">
            <i class="icon svg-slider-arrow-left svg-slider-arrow-left-dims"></i>
        </button>

        <button class="slick-arrow c-slider-teachers__arrow c-slider-teachers__arrow--next js-change-slide" data-slider="#slider-teachers" data-action="slickNext">
            <i class="icon svg-slider-arrow-right svg-slider-arrow-right-dims"></i>
        </button>
    </div>
</div>

<?php 
$crewCategory = get_field('crew__category');
$crew = get_posts([
    'post_type' => 'crew',
    'tax_query' => [
        [
            'taxonomy' => 'crew-category',
            'field'    => 'slug',
            'terms'    => $crewCategory->slug,
        ],
    ],            
]); ?>

<div class="c-slider-teachers" id="slider-teachers">
    <?php foreach($crew as $post): setup_postdata($post); ?>
        <div class="c-slider-teachers__slide">
            <?php if($crewCategory->slug == 'artists'): ?>
                <div class="c-card-person c-card-person--artist">
                    <div class="c-card-person__content">
                        <div class="c-card-person__image" style="background-image: url('<?php echo get_thumbnail_src('medium'); ?>');"></div>

                        <div class="c-card-person__description-short">
                            <?php the_field('description-short'); ?>
                        </div>

                        <div class="c-card-person__name">
                            <?php the_title(); ?>
                        </div>
                    </div>
                </div>                            
            <?php else: ?>
                <?php $title = explode(' ', get_the_title()); ?>

                <div class="c-card-person">
                    <div class="c-card-person__content">
                        <div class="c-card-person__image" style="background-image: url('<?php echo get_thumbnail_src('medium'); ?>');"></div>

                        <div class="c-card-person__name">
                            <span><?php echo $title[0]; ?></span> <?php echo $title[1]; ?>
                        </div>

                        <div class="c-card-person__description-short">
                            <?php the_field('description-short'); ?>
                        </div>

                        <div class="o-button-default c-card-person__button">
                            <?php _e('Читать подробнее', 'startime'); ?>
                        </div>
                    </div>

                    <div class="c-card-person__description">
                        <div class="c-card-person__name">
                            <span><?php echo $title[0]; ?></span> <?php echo $title[1]; ?>
                        </div>

                        <div class="c-card-person__description-short">
                            <?php the_field('description-short'); ?>
                        </div>

                        <div class="c-card-person__description-full">
                            <?php the_content(); ?>
                        </div>

                        <div class="c-card-person__description-back">
                            <i class="icon svg-card-arrow-left svg-card-arrow-left-dims"></i>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    <?php endforeach; wp_reset_postdata(); ?>
</div>
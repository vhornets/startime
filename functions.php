<?php
if (file_exists(dirname(__FILE__) . '/lib/vendor/autoload.php')) {
    require_once dirname(__FILE__) . '/lib/vendor/autoload.php';
}

require_once 'lib/metaboxes.php';
require_once 'lib/admin-options.php';
require_once 'lib/widgets.php';
require_once 'lib/utils.php';
require_once 'lib/filters.php';
require_once 'lib/actions.php';
require_once 'lib/post-types.php';
require_once 'lib/shortcodes.php';

add_image_size('hero-image', 455, 645, true);
add_image_size('service-thumb-additional', 345, 205, true);
add_image_size('gallery-image', 1245, 800, true);

add_theme_support('post-thumbnails');

register_nav_menus(array(
    'main-menu'            => 'Главное меню',
    'main-menu-secondary'  => 'Главное меню (второй ряд)',
    'archive-project-menu' => 'Меню продюсерского центра',
));

register_sidebar(array(
	'name' => 'Main sidebar',
	'id' => 'sidebar-main',
	'description' => 'Sidebar'
));

//Заголовки
add_action('template_redirect', 'last_mod_header');

function last_mod_header($headers) {
    if( is_singular() ) {
        $post_id = get_queried_object_id();
        $LastModified = gmdate("D, d M Y H:i:s \G\M\T", $post_id);
        $LastModified_unix = gmdate("D, d M Y H:i:s \G\M\T", $post_id);
        $IfModifiedSince = false;
        if( $post_id ) {
            if (isset($_ENV['HTTP_IF_MODIFIED_SINCE']))
                $IfModifiedSince = strtotime(substr($_ENV['HTTP_IF_MODIFIED_SINCE'], 5));
            if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']))
                $IfModifiedSince = strtotime(substr($_SERVER['HTTP_IF_MODIFIED_SINCE'], 5));
            if ($IfModifiedSince && $IfModifiedSince >= $LastModified_unix) {
                header($_SERVER['SERVER_PROTOCOL'] . ' 304 Not Modified');
                exit;
            }
    header("Last-Modified: " . get_the_modified_time("D, d M Y H:i:s", $post_id) );

    
            }
    }

}
// add_action('parse_request', 'parseUppercase');

// function parseUppercase($wp) {
//     // only process requests with "lid"
//     $query = $wp->query_vars;
//     $query = $query['pagename'];
//     if(isPartUppercase($query))
//     {
//         $link = site_url().'/'.strtolower($query);
//         wp_redirect( $link , 301 );

//         exit;
//     }
// }

// function isPartUppercase($string) {
//  return (bool) preg_match('/[A-Z]/', $string);
// }

function get_crumb_array(){
 
    /*
    echo '<xmp>';
    var_export(yoast_breadcrumb('', '', false));
    echo '</xmp>';
    */
    global $wp;
    
    $crumb = array();
    
    //Get all preceding links before the current page
    $dom = new DOMDocument();
   
    $html = mb_convert_encoding(yoast_breadcrumb('', '', false), 'HTML-ENTITIES', "UTF-8");
   
    $dom->loadHTML($html);
    $items = $dom->getElementsByTagName('a');
    
    foreach ($items as $tag)
    $crumb[] =  array('text' => $tag->nodeValue, 'href' => $tag->getAttribute('href')); 
    
    //Get the current page text and href 
    $items = new DOMXpath($dom);
    $dom = $items->query('//*[contains(@class, "breadcrumb_last")]');
    $crumb[] = array('text' => $dom->item(0)->nodeValue, 'href' => trailingslashit(home_url($wp->request)));
    return $crumb;
   }
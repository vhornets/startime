<?php get_header(); ?>

<?php if(have_posts()): the_post(); ?>

<section class="c-section-hero">
    <div class="c-slider-hero__images">
        <img src="<?php echo get_field('hero__image')['url']; ?>" alt="" class="c-slider-hero__image">

        <img src="<?php bloginfo('template_url'); ?>/img/decor1.svg" alt="" class="c-slider-hero__decor c-slider-hero__decor--1">
        <img src="<?php bloginfo('template_url'); ?>/img/decor2.svg" alt="" class="c-slider-hero__decor c-slider-hero__decor--2">
        <img src="<?php bloginfo('template_url'); ?>/img/decor3.svg" alt="" class="c-slider-hero__decor c-slider-hero__decor--3">
        <img src="<?php bloginfo('template_url'); ?>/img/decor4.svg" alt="" class="c-slider-hero__decor c-slider-hero__decor--4">
    </div>

    <div class="l-container">
        <div class="c-slider-hero">
            <div class="c-slider-hero__slide">
                <div class="o-badge c-slider-hero__badge">
                    <?php the_field('hero__badge'); ?>
                </div>

                <h1 class="c-slider-hero__title">
                    <?php the_field('hero__title'); ?>
                </h1>

                <?php if(get_field('hero__countdown')): ?>
                    <div class="c-countdown c-countdown--purple c-slider-hero__countdown" data-date="<?php the_field('hero__countdown'); ?>">
                        <div class="c-countdown__title">
                            <?php the_field('hero__countdown-title'); ?>
                        </div>

                        <div class="c-countdown__digit">
                            <span class="c-countdown__value c-countdown__days">17</span>
                            <span class="c-countdown__text">Дней</span>
                        </div>

                        <div class="c-countdown__separator">:</div>

                        <div class="c-countdown__digit">
                            <span class="c-countdown__value c-countdown__hours">09</span>
                            <span class="c-countdown__text">Часов</span>
                        </div>

                        <div class="c-countdown__separator">:</div>

                        <div class="c-countdown__digit">
                            <span class="c-countdown__value c-countdown__minutes">45</span>
                            <span class="c-countdown__text">Минут</span>
                        </div>

                        <div class="c-countdown__separator">:</div>

                        <div class="c-countdown__digit">
                            <span class="c-countdown__value c-countdown__seconds">20</span>
                            <span class="c-countdown__text">Секунд</span>
                        </div>
                    </div>
                <?php endif; ?>

                <div>
                    <a href="<?php the_field('hero__button-href'); ?>" class="o-button-default c-slider-hero__button open-popup" data-open="entry">
                        <?php the_field('hero__button-text'); ?>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="c-section-about-course">
    <img src="<?php bloginfo('template_url'); ?>/img/about-course-bg-pattern.svg" class="c-section-about-course__bg" />
    <img src="<?php bloginfo('template_url'); ?>/img/about-course-decor.svg" class="c-section-about-course__decor" />

    <div class="l-about-course">
        <div class="c-about-course">
            <h2 class="c-about-course__title">
                <?php the_field('about-course__title'); ?>
            </h2>

            <div class="c-about-course__text">
                <?php echo apply_filters( 'the_content', get_field('about-course__text') ); ?>
            </div>

            <h5 class="c-about-course__title2">
                <?php the_field('about-course__list-title'); ?>
            </h5>

            <div class="c-about-course__subtitle">
                <?php the_field('about-course__list-subtitle'); ?>
            </div>

            <ul class="c-list-icons">
                <?php foreach(get_field('about-course__list') as $item): ?>
                    <li>
                        <div class="c-list-icons__icon">
                            <img src="<?php echo $item['icon']['sizes']['medium']; ?>" title="<?php echo $item['title']; ?>" alt="<?php echo $item['title']; ?>"/>
                        </div>

                        <div class="c-list-icons__title">
                            <?php echo $item['title']; ?>
                        </div>
                    </li>
                <?php endforeach; ?>
            </ul>

            <div class="c-about-course__footer">
                <?php the_field('about-course__text-footer'); ?>
            </div>
        </div>
    </div>
</section>

<section class="c-section-prices">
    <?php $prices = get_field('prices'); ?>

    <img src="<?php bloginfo('template_url'); ?>/img/prices-bg-pattern.svg" class="c-section-prices__bg" />

    <div class="l-container">
        <h2 class="text-center">
            <?php the_field('prices__title'); ?>
        </h2>
    </div>

    <?php if(count($prices) > 1): ?>
        <div class="l-prices hidden--mobile">
            <?php get_template_part('partials/course-prices'); ?>
        </div>
        <div class="l-prices visible--mobile">
            <div class="c-slider-news" id="c-slider-prices">
                <?php get_template_part('partials/course-prices'); ?>
            </div>

            <div class="slide-nav-arrows">
                <button class="slide-nav-mob slide-nav-mob--prev js-change-slide" data-slider="#c-slider-prices" data-action="slickPrev">
                    <?php echo file_get_contents( get_bloginfo('template_url') . '/img/templates/slider-arrow.svg' ); ?>
                </button>

                <button class="slide-nav-mob slide-nav-mob--next js-change-slide" data-slider="#c-slider-prices" data-action="slickNext">
                    <?php echo file_get_contents( get_bloginfo('template_url') . '/img/templates/slider-arrow.svg' ); ?>
                </button>
            </div>
        </div>
    <?php endif; ?>

    <?php if(count($prices) === 1): ?>
        <div class="l-prices">
            <?php get_template_part('partials/course-prices-single'); ?>
        </div>
    <?php endif; ?>
</section>

<section class="c-section-form">
    <?php get_template_part('partials/form-horizontal'); ?>
</section>

<section class="c-section-reasons">
    <img src="<?php bloginfo('template_url'); ?>/img/reasons-bg-pattern.svg" class="c-section-reasons__bg" />

    <?php get_template_part('partials/course-reasons'); ?>
</section>

<section class="c-section-steps">
    <div class="l-container">
        <h2 class="text-center">
            <?php the_field('course-steps__title'); ?>
        </h2>
    </div>

    <div class="l-steps">
        <?php foreach(get_field('course-steps') as $item): ?>
            <div class="l-steps__item">
                <div class="c-card-star">
                    <img src="<?php bloginfo('template_url'); ?>/img/star.svg" class="c-card-star__image" />

                    <div class="c-card-star__text">
                        <?php echo $item['title']; ?>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</section>

<section class="c-section-teachers">
    <?php if(get_field('crew__display') != 'hide'): ?>
        <img src="<?php bloginfo('template_url'); ?>/img/teachers-bg-pattern.svg" class="c-section-teachers__bg" />

        <?php get_template_part('partials/crew-single-category'); ?>

        <!-- <div class="c-slider-teachers__button">
            <button class="o-button-default">
                <?php the_field('crew__button-text'); ?>
            </button>
        </div> -->
    <?php endif; ?>
</section>

<section class="c-section-photos">
    <?php get_template_part('partials/gallery'); ?>
</section>

<section class="c-section-video">
    <?php get_template_part('partials/video'); ?>
</section>

<section class="c-section-form">
    <img src="<?php bloginfo('template_url'); ?>/img/form-bg-pattern.svg" class="c-section-form__bg" />

    <?php get_template_part('partials/form-horizontal'); ?>
</section>

<?php endif; ?>

<?php get_footer(); ?>

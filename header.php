<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <title><?php echo get_bloginfo('name') . wp_title(' > ', false); ?></title>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0" />

    <?php if (has_site_icon()): ?>
        <?php wp_site_icon(); ?>
    <?php else: ?>
        <link type="image/x-icon" rel="icon" href="<?php echo bloginfo('template_url'); ?>/favicon.ico">
    <?php endif; ?>

    <?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); ?>

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<section id="popup">
    <div class="popup__close">
        <span></span>
        <span></span>
    </div>
    
    <div class="popup-container">
        <?php get_template_part('partials/form', 'horizontal'); ?>
    </div>
</section>

<div class="c-main-menu-mobile__wrap">
    <img src="<?php bloginfo('template_url'); ?>/img/templates/menu-mob-bg.svg" class="c-main-menu-mobile__bg" />
    <div class="l-header__mobile">
        <div class="l-header__socials">
            <ul class="c-socials">
                <?php get_template_part('partials/socials'); ?>
            </ul>
        </div>

        <div class="l-header__languages">
            <?php //qtranxf_generateLanguageSelectCode(); ?>
        </div>

        <div class="l-header__contacts">
            <a href="tel:<?php the_field('phone', 'options'); ?>" class="c-header__phone">
                <i class="icon svg-phone svg-phone-dims"></i>
                <?php the_field('phone', 'options'); ?>
            </a>

            <a href="http://maps.google.com/?q=<?php the_field('address', 'options'); ?>" class="c-header__address" target="_blank" rel="nofollow">
                <i class="icon svg-marker svg-marker-dims"></i>
                <?php the_field('address', 'options'); ?>
            </a>
        </div>

    </div>

    <div class="c-main-menu-mobile__content">
        <ul class="c-main-menu-mobile">
            <?php wp_nav_menu(array(
                'menu' => 'main-menu',
                'theme_location' => 'main-menu',
                'container' => false,
                'items_wrap' => '%3$s',
            )); ?>

            <?php wp_nav_menu(array(
                'menu' => 'main-menu-secondary',
                'theme_location' => 'main-menu-secondary',
                'container' => false,
                'items_wrap' => '%3$s',
                'menu_class' => 'c-main-menu',
                'walker' => new Vh_Walker_Menu()
            )); ?>
        </ul>

        <a href="#" class="o-button-default">Написать нам</a>
    </div>
</div>

<header class="c-header">
    <div class="l-topbar">
        <?php wp_nav_menu(array(
            'menu' => 'main-menu',
            'theme_location' => 'main-menu',
            'container' => false,
            'menu_class' => 'c-main-menu-topbar',
            'walker' => new Vh_Walker_Menu()
        )); ?>
    </div>

    <div class="c-header__main">
        <div class="l-header">
            <div class="l-header__menu">
                <div class="l-header__menu-toggle">
                    <button type="button" class="c-main-menu__toggle js-toggle-main-menu">
                        <span></span>
                        <span></span>
                        <span></span>
                    </button>
                </div>
                <div class="l-header__logo">
                    <a href="<?php echo get_option('home'); ?>" class="c-header__logo">
                        <?php echo file_get_contents( get_field('logo', 'options')['url'] ); ?>
                    </a>
                </div>

                <div class="l-header__navigation">
                    <?php wp_nav_menu(array(
                        'menu' => 'main-menu-secondary',
                        'theme_location' => 'main-menu-secondary',
                        'container' => false,
                        'menu_class' => 'c-main-menu',
                        'walker' => new Vh_Walker_Menu()
                    )); ?>
                </div>

                <div class="l-header__audio">
                    <audio id="player" controls>
                        <source src="http://85.238.98.185:5000/live" type="audio/mpeg" />
                    </audio>
                </div>
            </div>

            <div class="l-header__info">
                <div class="l-header__socials">
                    <ul class="c-socials">
                        <?php get_template_part('partials/socials'); ?>
                    </ul>
                </div>

                <div class="l-header__contacts">
                    <a href="tel:<?php the_field('phone', 'options'); ?>" class="c-header__phone">
                        <i class="icon svg-phone svg-phone-dims"></i>
                        <?php the_field('phone', 'options'); ?>
                    </a>

                    <a href="http://maps.google.com/?q=<?php the_field('address', 'options'); ?>" class="c-header__address" target="_blank" rel="nofollow">
                        <i class="icon svg-marker svg-marker-dims"></i>
                        <?php the_field('address', 'options'); ?>
                    </a>
                </div>

                <div class="l-header__languages">
                    <?php //qtranxf_generateLanguageSelectCode(); ?>
                </div>
            </div>
        </div>
    </div>

    <?php
if ( function_exists('yoast_breadcrumb') && !is_front_page()) {
yoast_breadcrumb( '<p id="breadcrumbs" class="l-container">','</p>' );

$bread_list = get_crumb_array();

$micro_list = [];
foreach($bread_list as $pos => $item){
    $micro_list[$pos] = '{
        "@type": "ListItem",
        "position": '.$pos.',
        "item": {
            "@id": "'.$item["href"].'",
            "name": "'.$item["text"].'"
        }
    }';
}

$micro_list = implode(',', $micro_list);
?>

<script type="application/ld+json">
{
    "@context": "http://schema.org",
    "@type": "BreadcrumbList",
    "itemListElement": [
        <?=$micro_list; ?>
    ]
}
</script>

<?php
}?>
</header>

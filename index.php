<?php get_header(); ?>

<section class="c-section-main-bg js-tabs">
    <img src="<?php bloginfo('template_url'); ?>/img/templates/news/news_bg.png" class="camp_main__img" />
    <img src="<?php bloginfo('template_url'); ?>/img/templates/camp/camp_main_photo_bg.svg" class="camp_main__bg" />

    <div class="l-position text-center">
        <div class="l-container text-color">
            <h2 class="c-section-main-bg__title">
                Наши новости
            </h2>

            <ul class="c-tabs-nav hidden--mobile">
                <li>
                    <a href="<?php echo get_the_permalink( get_option('page_for_posts') ); ?>" <?php if(is_home()): ?>class="is-active"<?php endif; ?>>
                        Все
                    </a>
                </li>

                <?php wp_get_archives (['type' => 'yearly']) ?>
            </ul>

            <ul class="c-tabs-nav visible--mobile">
                <button class="c-tabs-nav--arrow js-change-slide" data-slider="#tabs-slider" data-action="slickPrev">
                    <?php echo file_get_contents( get_bloginfo('template_url') . '/img/templates/tabs-arrow.svg' ); ?>
                </button>
                <div class="c-slider-tabs-mobile" id="tabs-slider">
                    <li>
                        <a href="<?php echo get_the_permalink( get_option('page_for_posts') ); ?>" <?php if(is_home()): ?>class="is-active"<?php endif; ?>>
                            Все
                        </a>
                    </li>
                    <?php wp_get_archives (['type' => 'yearly']) ?>
                </div>

                <button class="c-tabs-nav--arrow c-tabs-nav--arrow-right js-change-slide" data-slider="#tabs-slider" data-action="slickNext">
                    <?php echo file_get_contents( get_bloginfo('template_url') . '/img/templates/tabs-arrow.svg' ); ?>
                </button>
            </ul>
        </div>
    </div>
</section>

<section class="c-section-news">
    <div class="l-container js-blog-posts">
        <div class="news-block__data">
            <?php if(have_posts()): while(have_posts()): the_post(); ?>
                <?php get_template_part('partials/news-card'); ?>
            <?php endwhile; endif; ?>
        </div>
    </div>

    <div class="text-center js-blog-pagination-container">
        <?php next_posts_link( __('Показать еще', 'startime') ); ?>
    </div>
</section>


<?php get_footer(); ?>

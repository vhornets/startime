<?php
function request_init() {
    register_post_type('request', [
        'labels'            => ['menu_name' => 'Заявки', 'singular_name' => 'Заявка'],
        'menu_position'     => 24,
        'public'            => false,
        'hierarchical'      => false,
        'show_ui'           => true,
        'show_in_nav_menus' => true,
        'supports'          => ['title', 'editor'],
        'has_archive'       => true,
        'rewrite'           => true,
        'query_var'         => true,
        'menu_icon'         => 'dashicons-phone',
        'show_in_rest'      => true,
        'rest_base'         => 'request',
        'rest_controller_class' => 'WP_REST_Posts_Controller',
    ]);
}
add_action( 'init', 'request_init' );

function course_init() {
    register_post_type('course', [
        'labels'            => ['menu_name' => 'Направления', 'name' => 'Направления', 'singular_name' => 'Направление'],
        'menu_position'     => 20,
        'public'            => true,
        'hierarchical'      => false,
        'show_ui'           => true,
        'show_in_nav_menus' => true,
        'supports'          => ['title', 'editor', 'thumbnail'],
        'has_archive'       => true,
        'rewrite' => ['slug' => 'schoolcourses','with_front' => false],
        'query_var'         => true,
        'menu_icon'         => 'dashicons-welcome-learn-more',
        'show_in_rest'      => true,
        'rest_base'         => 'course',
        'rest_controller_class' => 'WP_REST_Posts_Controller',
    ]);

}
add_action( 'init', 'course_init' );


function course_category_init() {
    register_taxonomy('course-type', ['course'], [
        'hierarchical'      => true,
        'public'            => true,
        'show_in_nav_menus' => true,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => true,
        'labels'            => [
            'name' => 'Тип направления',
            'singular_name' => 'Тип направления',
        ],
    ]);

}
add_action( 'init', 'course_category_init' );

function crew_init() {
    register_post_type('crew', [
        'labels'            => ['menu_name' => 'Команда', 'name' => 'Команда', 'singular_name' => 'Член команды'],
        'menu_position'     => 20,
        'public'            => true,
        'hierarchical'      => false,
        'show_ui'           => true,
        'show_in_nav_menus' => true,
        'supports'          => ['title', 'editor', 'thumbnail'],
        'has_archive'       => true,
        'rewrite'           => true,
        'query_var'         => true,
        'menu_icon'         => 'dashicons-buddicons-buddypress-logo',
        'show_in_rest'      => true,
        'rest_base'         => 'crew',
        'rest_controller_class' => 'WP_REST_Posts_Controller',
    ]);

}
add_action( 'init', 'crew_init' );

function crew_category_init() {
    register_taxonomy('crew-category', ['crew'], [
        'hierarchical'      => true,
        'public'            => true,
        'show_in_nav_menus' => true,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => true,
        'labels'            => [
            'name' => 'Категории',
            'singular_name' => 'Категория',
        ],
    ]);

}
add_action( 'init', 'crew_category_init' );

function project_init() {
    register_post_type('project', [
        'labels'            => ['menu_name' => 'Проекты', 'name' => 'Проекты', 'singular_name' => 'Проект'],
        'menu_position'     => 20,
        'public'            => true,
        'hierarchical'      => false,
        'show_ui'           => true,
        'show_in_nav_menus' => true,
        'supports'          => ['title', 'editor', 'thumbnail'],
        'has_archive'       => true,
        'rewrite'           => true,
        'query_var'         => true,
        'menu_icon'         => 'dashicons-buddicons-topics',
        'show_in_rest'      => true,
        'rest_base'         => 'project',
        'rest_controller_class' => 'WP_REST_Posts_Controller',
    ]);

}
add_action( 'init', 'project_init' );

function project_category_init() {
    register_taxonomy('project-category', ['project'], [
        'hierarchical'      => true,
        'public'            => true,
        'show_in_nav_menus' => true,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => true,
        'labels'            => [
            'name' => 'Категории проектов',
            'singular_name' => 'Категория проектов',
        ],
    ]);

}
add_action( 'init', 'project_category_init' );

function nominee_init() {
    register_post_type('nominee', [
        'labels'            => ['menu_name' => 'Номинанты', 'name' => 'Номинанты', 'singular_name' => 'Номинант'],
        'menu_position'     => 20,
        'public'            => true,
        'hierarchical'      => false,
        'show_ui'           => true,
        'show_in_nav_menus' => true,
        'supports'          => ['title', 'editor', 'thumbnail'],
        'has_archive'       => true,
        'rewrite'           => true,
        'query_var'         => true,
        'menu_icon'         => 'dashicons-buddicons-tracking',
        'show_in_rest'      => true,
        'rest_base'         => 'nominee',
        'rest_controller_class' => 'WP_REST_Posts_Controller',
    ]);

}
add_action( 'init', 'nominee_init' );

function review_init() {
    register_post_type('review', [
        'labels'            => ['menu_name' => 'Отзывы', 'name' => 'Отзывы', 'singular_name' => 'Отзыв'],
        'menu_position'     => 20,
        'public'            => true,
        'hierarchical'      => false,
        'show_ui'           => true,
        'show_in_nav_menus' => true,
        'supports'          => ['title', 'editor', 'thumbnail'],
        'has_archive'       => true,
        'query_var'         => true,
        'menu_icon'         => 'dashicons-feedback',
        'show_in_rest'      => true,
        'rest_base'         => 'review',
        'rest_controller_class' => 'WP_REST_Posts_Controller',
    ]);

}
add_action( 'init', 'review_init' );

function review_type_init() {
    register_taxonomy('type', ['review'], [
        'hierarchical'      => true,
        'public'            => true,
        'show_in_nav_menus' => true,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => true,
        'labels'            => [
            'name' => 'Тип отзыва',
            'singular_name' => 'Тип отзыва',
        ],
    ]);

}
add_action( 'init', 'review_type_init' );
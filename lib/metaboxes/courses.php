<?php
acf_add_local_field_group([
    'key' => 'group_course',
    'title' => 'Общие настройки',
    'fields' => [
        [
            'key' => 'field_course_color_from', 
            'label' => 'Градиент - от',
            'name' => 'color-from',
            'type' => 'color_picker',
        ],
        [
            'key' => 'field_course_color_to',
            'label' => 'Градиент - до',
            'name' => 'color-to',
            'type' => 'color_picker',
        ],
    ],
    'location' => [
        [
            [
                'param' => 'post_type',
                'operator' => '==',
                'value' => "course",
            ],
        ],
    ]
]);

acf_add_local_field_group([
    'key' => 'group_course_hero',
    'title' => 'Шапка',
    'fields' => [
        [
            'key' => 'field_course_hero_slide_image',
            'label' => 'Изображение',
            'name' => 'hero__image',
            'type' => 'image',
        ],
        [
            'key' => 'field_course_hero_slide_badge',
            'label' => 'Лейбл',
            'name' => 'hero__badge',
            'type' => 'qtranslate_text',
        ],
        [
            'key' => 'field_course_hero_slide_title',
            'label' => 'Заголовок',
            'name' => 'hero__title',
            'type' => 'qtranslate_textarea',
            'new_lines' => 'br',
            'rows' => 2,
        ],
        [
            'key' => 'field_course_hero_slide_countdown_title',
            'label' => 'Заголовок счетчика',
            'name' => 'hero__countdown-title',
            'type' => 'qtranslate_text',
        ],
        [
            'key' => 'field_course_hero_slide_countdown',
            'label' => 'Счетчик',
            'name' => 'hero__countdown',
            'type' => 'date_picker',
            'display_format' => 'm/d/Y',
            'return_format' => 'm/d/Y',
        ],
        [
            'key' => 'field_course_hero_slide_button_text',
            'label' => 'Текст кнопки',
            'name' => 'hero__button-text',
            'type' => 'qtranslate_text',
        ],
        [
            'key' => 'field_course_hero_slide_button_href',
            'label' => 'Ссылка кнопки',
            'name' => 'hero__button-href',
            'type' => 'qtranslate_text',
        ],
    ],
    'location' => [
        [
            [
                'param' => 'post_type',
                'operator' => '==',
                'value' => "course",
            ],
        ],
    ]
]);

acf_add_local_field_group([
    'key' => 'group_about_course',
    'title' => 'О направлении',
    'fields' => [
        [
            'key' => 'field_about_course_title',
            'label' => 'Заголовок',
            'name' => 'about-course__title',
            'type' => 'qtranslate_text',
        ],
        [
            'key' => 'field_about_course_text',
            'label' => 'Текст',
            'name' => 'about-course__text',
            'type' => 'qtranslate_wysiwyg',
            'default_value' => '',
            'tabs' => 'all',
            'toolbar' => 'basic',
            'media_upload' => 0,
            'delay' => 0,
        ],
        [
            'key' => 'field_about_course_list_title',
            'label' => 'Заголовок списка',
            'name' => 'about-course__list-title',
            'type' => 'qtranslate_text',
        ],
        [
            'key' => 'field_about_course_list_subtitle',
            'label' => 'Подзаголовок списка',
            'name' => 'about-course__list-subtitle',
            'type' => 'qtranslate_text',
        ],
        [
            'key' => 'field_about_course_list',
            'label' => 'Список',
            'type' => 'repeater',
            'layout' => 'block',
            'name' => 'about-course__list',
            'sub_fields' => [
                [
                    'key' => 'field_about_course_list_item_icon',
                    'label' => 'Иконка',
                    'name' => 'icon',
                    'type' => 'image',
                ],
                [
                    'key' => 'field_about_course_list_item_title',
                    'label' => 'Заголовок',
                    'name' => 'title',
                    'type' => 'qtranslate_text',
                ],
            ]
        ],
        [
            'key' => 'field_about_course_text_footer',
            'label' => 'Текст внизу',
            'name' => 'about-course__text-footer',
            'type' => 'qtranslate_textarea',
            'new_lines' => 'br',
            'rows' => 2,
        ],  
    ],
    'location' => [
        [
            [
                'param' => 'post_type',
                'operator' => '==',
                'value' => "course",
            ],
        ],
    ]
]);

acf_add_local_field_group([
    'key' => 'group_course_prices',
    'title' => 'Цены',
    'fields' => [
        [
            'key' => 'field_prices_title',
            'label' => 'Заголовок',
            'name' => 'prices__title',
            'type' => 'qtranslate_text',
        ],
        [
            'key' => 'field_prices',
            'label' => 'Цены',
            'type' => 'repeater',
            'layout' => 'block',
            'name' => 'prices',
            'sub_fields' => [
                [
                    'key' => 'field_prices_item_duration',
                    'label' => 'Продолжительность',
                    'name' => 'duration',
                    'type' => 'qtranslate_text',
                ],                [
                    'key' => 'field_prices_item_title',
                    'label' => 'Заголовок',
                    'name' => 'title',
                    'type' => 'qtranslate_text',
                ],
                [
                    'key' => 'field_prices_item_subtitle',
                    'label' => 'Подзаголовок',
                    'name' => 'subtitle',
                    'type' => 'qtranslate_text',
                ],
                [
                    'key' => 'field_prices_item_list_title',
                    'label' => 'Заголовок списка',
                    'name' => 'list-title',
                    'type' => 'qtranslate_text',
                ],
                [
                    'key' => 'field_prices_item_list',
                    'label' => 'Список',
                    'type' => 'repeater',
                    'layout' => 'block',
                    'name' => 'list',
                    'sub_fields' => [
                        [
                            'key' => 'field_prices_item_list_item_title',
                            'label' => 'Заголовок',
                            'name' => 'title',
                            'type' => 'qtranslate_text',
                        ],
                    ]
                ], 
                [
                    'key' => 'field_prices_item_price',
                    'label' => 'Цена',
                    'name' => 'price',
                    'type' => 'qtranslate_text',
                ],
                [
                    'key' => 'field_prices_item_footnote',
                    'label' => 'Сноска внизу',
                    'name' => 'footnote',
                    'type' => 'qtranslate_text',
                ],
                [
                    'key' => 'field_prices_item_button_text',
                    'label' => 'Текст кнопки',
                    'name' => 'button-text',
                    'type' => 'qtranslate_text',
                ],
                [
                    'key' => 'field_prices_item_button_href',
                    'label' => 'Ссылка кнопки',
                    'name' => 'button-href',
                    'type' => 'qtranslate_text',
                ],
            ]
        ],
    ],
    'location' => [
        [
            [
                'param' => 'post_type',
                'operator' => '==',
                'value' => "course",
            ],
        ],
    ]
]);

acf_add_local_field_group([
    'key' => 'group_form',
    'title' => 'Форма',
    'fields' => [
        [
            'key' => 'field_form_title',
            'label' => 'Заголовок',
            'name' => 'form__title',
            'type' => 'qtranslate_textarea',
            'new_lines' => 'br',
            'rows' => 2,
        ],
        [
            'key' => 'field_form_form_title',
            'label' => 'Заголовок формы',
            'name' => 'form__form-title',
            'type' => 'qtranslate_text',
        ],
        [
            'key' => 'field_form_form_text',
            'label' => 'Текст формы',
            'name' => 'form__form-text',
            'type' => 'qtranslate_textarea',
            'new_lines' => 'br',
            'rows' => 2,
        ],
    ],
    'location' => [
        [
            [
                'param' => 'post_type',
                'operator' => '==',
                'value' => 'course',
            ],
        ],
        [
            [
                'param' => 'post_template',
                'operator' => '==',
                'value' => 'templates/camp.php',
            ],
        ],
        [
            [
                'param' => 'post_template',
                'operator' => '==',
                'value' => 'templates/home.php',
            ],
        ],
        [
            [
                'param' => 'post_type',
                'operator' => '==',
                'value' => "project",
            ],
        ],
        [
            [
                'param' => 'post_template',
                'operator' => '==',
                'value' => "templates/faq.php",
            ],
        ],
        [
            [
                'param' => 'post_template',
                'operator' => '==',
                'value' => "templates/vacancies.php",
            ],
        ],
    ]
]);

acf_add_local_field_group([
    'key' => 'group_course_reasons',
    'title' => 'Приичны учиться в Startime / Список на странице вакансий',
    'fields' => [
        [
            'key' => 'field_course_reasons_title',
            'label' => 'Заголовок',
            'name' => 'course-reasons__title',
            'type' => 'qtranslate_textarea',
            'new_lines' => 'br',
            'rows' => 1,
        ],
        [
            'key' => 'field_course_reasons_list',
            'label' => 'Список',
            'type' => 'repeater',
            'layout' => 'block',
            'name' => 'course-reasons',
            'sub_fields' => [
                [
                    'key' => 'field_course_reasons_list_item_title',
                    'label' => 'Заголовок',
                    'name' => 'title',
                    'type' => 'qtranslate_textarea',
                    'new_lines' => 'br',
                    'rows' => 2,
                ],
            ]
        ],
    ],
    'location' => [
        [
            [
                'param' => 'post_type',
                'operator' => '==',
                'value' => "course",
            ],
        ],
        [
            [
                'param' => 'post_template',
                'operator' => '==',
                'value' => "templates/vacancies.php",
            ],
        ],
    ]
]);

acf_add_local_field_group([
    'key' => 'group_course_steps',
    'title' => 'Как проходит обучение',
    'fields' => [
        [
            'key' => 'field_course_steps_title',
            'label' => 'Заголовок',
            'name' => 'course-steps__title',
            'type' => 'qtranslate_text',
        ],
        [
            'key' => 'field_course_steps_list',
            'label' => 'Список',
            'type' => 'repeater',
            'layout' => 'block',
            'name' => 'course-steps',
            'sub_fields' => [
                [
                    'key' => 'field_course_steps_list_item_title',
                    'label' => 'Заголовок',
                    'name' => 'title',
                    'type' => 'qtranslate_text',
                ],
            ]
        ],
    ],
    'location' => [
        [
            [
                'param' => 'post_type',
                'operator' => '==',
                'value' => "course",
            ],
        ],
    ]
]);

acf_add_local_field_group([
    'key' => 'group_crew',
    'title' => 'Команда (преподаватели/артисты)',
    'fields' => [
        [
            'key' => 'field_crew_hide',
            'label' => 'Отображение блока',
            'type' => 'radio',
            'name' => 'crew__display',
            'choices' => [
                'show' => 'Показывать',
                'hide' => 'Скрывать'
            ]
        ],
        [
            'key' => 'field_crew_title',
            'label' => 'Заголовок',
            'name' => 'crew__title',
            'type' => 'qtranslate_text',
        ],
        [
            'key' => 'field_crew',
            'label' => 'Отображаемая категория',
            'name' => 'crew__category',
            'field_type' => 'radio',
            'type' => 'taxonomy',
            'taxonomy' => 'crew-category',
            'return_format' => 'object',
            'multiple' => 0,
        ],
        [
            'key' => 'field_crew_button_text',
            'label' => 'Текст кнопки',
            'name' => 'crew__button-text',
            'type' => 'qtranslate_text',
        ],
    ],
    'location' => [
        [
            [
                'param' => 'post_type',
                'operator' => '==',
                'value' => "course",
            ],
        ],
        [
            [
                'param' => 'post_type',
                'operator' => '==',
                'value' => "project",
            ],
        ],
    ]
]);

acf_add_local_field_group([
    'key' => 'group_gallery',
    'title' => 'Галерея',
    'fields' => [
        [
            'key' => 'field_gallery_hide',
            'label' => 'Отображение блока',
            'type' => 'radio',
            'name' => 'gallery__display',
            'choices' => [
                'show' => 'Показывать',
                'hide' => 'Скрывать'
            ]
        ],
        [
            'key' => 'field_gallery_title',
            'label' => 'Заголовок',
            'name' => 'gallery__title',
            'type' => 'qtranslate_text',
        ],
        [
            'key' => 'field_gallery',
            'label' => 'Галерея',
            'name' => 'gallery',
            'type' => 'gallery',
        ],
    ],
    'location' => [
        [
            [
                'param' => 'post_type',
                'operator' => '==',
                'value' => "course",
            ],
        ],
        [
            [
                'param' => 'post_template',
                'operator' => '==',
                'value' => 'templates/camp.php',
            ],
        ],
        [
            [
                'param' => 'post_type',
                'operator' => '==',
                'value' => "project",
            ],
        ],
    ]
]);

acf_add_local_field_group([
    'key' => 'group_videos',
    'title' => 'Видео',
    'fields' => [
        [
            'key' => 'field_videos_hide',
            'label' => 'Отображение блока',
            'type' => 'radio',
            'name' => 'videos__display',
            'choices' => [
                'show' => 'Показывать',
                'hide' => 'Скрывать'
            ]
        ],
        [
            'key' => 'field_videos_title',
            'label' => 'Заголовок',
            'name' => 'course-steps__title',
            'type' => 'qtranslate_text',
        ],
        [
            'key' => 'field_videos',
            'label' => 'Видео',
            'type' => 'repeater',
            'layout' => 'block',
            'name' => 'videos',
            'sub_fields' => [
                [
                    'key' => 'field_videos_item_image',
                    'label' => 'Обложка',
                    'name' => 'cover',
                    'type' => 'image',
                ],
                [
                    'key' => 'field_videos_item_title',
                    'label' => 'Заголовок',
                    'name' => 'title',
                    'type' => 'qtranslate_text',
                ],
                [
                    'key' => 'field_videos_item_video',
                    'label' => 'Ссылка на Youtube-видео',
                    'name' => 'video',
                    'type' => 'oembed',
                ],
            ]
        ],
    ],
    'location' => [
        [
            [
                'param' => 'post_type',
                'operator' => '==',
                'value' => "course",
            ],
        ],
        [
            [
                'param' => 'post_template',
                'operator' => '==',
                'value' => 'templates/camp.php',
            ],
        ],
        [
            [
                'param' => 'post_type',
                'operator' => '==',
                'value' => "project",
            ],
        ],
    ]
]);
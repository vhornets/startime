<?php
acf_add_local_field_group([
    'key' => 'group_nominee',
    'title' => 'Общие настройки',
    'fields' => [
        [
            'key' => 'field_nominee_age', 
            'label' => 'Возраст',
            'name' => 'age',
            'type' => 'qtranslate_text',
        ],
        [
            'key' => 'field_nominee_occupation', 
            'label' => 'Род деятельности',
            'name' => 'occupation',
            'type' => 'qtranslate_text',
        ],
        [
            'key' => 'field_nominee_description', 
            'label' => 'Короткое описание',
            'name' => 'description',
            'type' => 'qtranslate_textarea',
            'new_lines' => 'br',
            'rows' => 2,
        ],
        [
            'key' => 'field_nominee_votes', 
            'label' => 'Голоса',
            'name' => 'votes',
            'type' => 'number',
            'default_value' => 0
        ],
    ],
    'location' => [
        [
            [
                'param' => 'post_type',
                'operator' => '==',
                'value' => "nominee",
            ],
        ],
    ]
]);
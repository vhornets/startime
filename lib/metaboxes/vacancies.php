<?php
acf_add_local_field_group([
    'key' => 'group_vacancies',
    'title' => 'Вакансии',
    'fields' => [
        [
            'key' => 'field_vacancies',
            'label' => 'Слайдер вакансий',
            'type' => 'repeater',
            'layout' => 'block',
            'name' => 'vacancies',
            'sub_fields' => [
                [
                    'key' => 'field_vacancy',
                    'label' => 'Текст',
                    'name' => 'text',
                    'type' => 'qtranslate_textarea',
                    'new_lines' => 'br',
                ],             
            ],
        ]
    ],
    'location' => acf_get_template_location_array('vacancies')
]);

acf_add_local_field_group([
    'key' => 'group_vacancies_schedule',
    'title' => 'График работы',
    'fields' => [
        [
            'key' => 'field_vacancies_schedule',
            'label' => 'Изображение',
            'name' => 'vacancies-schedule__image',
            'type' => 'image',
        ],
        [
            'key' => 'field_vacancies_schedule_title',
            'label' => 'Заголовок',
            'name' => 'vacancies-schedule__title',
            'type' => 'qtranslate_textarea',
            'new_lines' => 'br',
            'rows' => 1,
        ],
        [
            'key' => 'field_vacancies_schedule_subtitle',
            'label' => 'Подзаголовок',
            'name' => 'vacancies-schedule__subtitle',
            'type' => 'qtranslate_textarea',
            'new_lines' => 'br',
            'rows' => 2,
        ],
    ],
    'location' => acf_get_template_location_array(['vacancies'])
]);

acf_add_local_field_group([
    'key' => 'group_vacancies_why',
    'title' => 'Почему стоит работать у нас',
    'fields' => [
        [
            'key' => 'field_vacancies_why_title',
            'label' => 'Заголовок',
            'name' => 'vacancies-why__title',
            'type' => 'qtranslate_textarea',
            'new_lines' => 'br',
            'rows' => 1,
        ],
        [
            'key' => 'field_vacancies_why_card',
            'label' => 'Карточки',
            'type' => 'repeater',
            'layout' => 'block',
            'name' => 'vacancies-why__cards',
            'sub_fields' => [
                [
                    'key' => 'field_vacancies_why_card_image',
                    'label' => 'Изобарежение',
                    'name' => 'image',
                    'type' => 'image',
                ],    
                [
                    'key' => 'field_vacancies_why_card_title',
                    'label' => 'Заголовок',
                    'name' => 'title',
                    'type' => 'qtranslate_textarea',
                    'new_lines' => 'br',
                    'rows' => 1,
                ],       
                [
                    'key' => 'field_vacancies_why_card_text',
                    'label' => 'Текст',
                    'name' => 'text',
                    'type' => 'qtranslate_textarea',
                    'new_lines' => 'br',
                    'rows' => 2,
                ],             
            ],
        ]
    ],
    'location' => acf_get_template_location_array('vacancies')
]);
<?php
acf_add_local_field_group([
    'key' => 'group_teleprojects_hero',
    'title' => 'Шапка',
    'fields' => [
        [
            'key' => 'field_teleprojects_hero_slide_image',
            'label' => 'Изображение',
            'name' => 'teleprojects-hero__image',
            'type' => 'image',
        ],
        [
            'key' => 'field_teleprojects_hero_slide_badge',
            'label' => 'Лейбл',
            'name' => 'teleprojects-hero__badge',
            'type' => 'qtranslate_text',
        ],
        [
            'key' => 'field_teleprojects_hero_slide_title',
            'label' => 'Заголовок',
            'name' => 'teleprojects-hero__title',
            'type' => 'qtranslate_textarea',
            'new_lines' => 'br',
            'rows' => 2,
        ],
        [
            'key' => 'field_teleprojects_hero_slide_subtitle',
            'label' => 'Подзаголовок',
            'name' => 'teleprojects-hero__subtitle',
            'type' => 'qtranslate_textarea',
            'new_lines' => 'br',
            'rows' => 2,
        ],
    ],
    'location' => acf_get_template_location_array('teleprojects')
]);

acf_add_local_field_group([
    'key' => 'group_nominees',
    'title' => 'Номинанты',
    'fields' => [
        [
            'key' => 'field_nominees_title',
            'label' => 'Заголовок',
            'name' => 'nominees__title',
            'type' => 'qtranslate_text',
        ],
        [
            'key' => 'field_nominees_hide',
            'label' => 'Отображение блока',
            'type' => 'radio',
            'name' => 'nominees__display',
            'choices' => [
                'show' => 'Показывать',
                'hide' => 'Скрывать'
            ]
        ],
    ],
    'location' => acf_get_template_location_array('teleprojects')
]);

acf_add_local_field_group([
    'key' => 'group_broadcasts',
    'title' => 'Трансляции',
    'fields' => [
        [
            'key' => 'field_broadcasts_title',
            'label' => 'Заголовок',
            'name' => 'broadcasts__title',
            'type' => 'qtranslate_textarea',
            'new_lines' => 'br',
            'rows' => 1,
        ],
        [
            'key' => 'field_broadcasts_logos',
            'label' => 'Логотипы',
            'type' => 'gallery',
            'name' => 'logos',
        ],
    ],
    'location' => acf_get_template_location_array('teleprojects')
]);

acf_add_local_field_group([
    'key' => 'group_nominations',
    'title' => 'Номинации',
    'fields' => [
        [
            'key' => 'field_nominations_title',
            'label' => 'Заголовок',
            'name' => 'nominations__title',
            'type' => 'qtranslate_textarea',
            'new_lines' => 'br',
            'rows' => 1,
        ],
        [
            'key' => 'field_nominations',
            'label' => 'Карточки',
            'type' => 'repeater',
            'layout' => 'block',
            'name' => 'nominations',
            'sub_fields' => [
                [
                    'key' => 'field_nomination_image',
                    'label' => 'Изображение',
                    'name' => 'image',
                    'type' => 'image',
                ],
                [
                    'key' => 'field_nomination_title',
                    'label' => 'Заголовок',
                    'name' => 'title',
                    'type' => 'qtranslate_text',
                ],
                [
                    'key' => 'field_nomination_text',
                    'label' => 'Текст',
                    'name' => 'text',
                    'type' => 'qtranslate_text',
                ],
                [
                    'key' => 'field_nomination_color_from',
                    'label' => 'Градиент - от ',
                    'name' => 'color-from',
                    'type' => 'color_picker',
                ],
                [
                    'key' => 'field_nomination_color_to',
                    'label' => 'Градиент - до ',
                    'name' => 'color-to',
                    'type' => 'color_picker',
                ],
                [
                    'key' => 'field_nomination_list',
                    'label' => 'Список',
                    'type' => 'repeater',
                    'layout' => 'block',
                    'name' => 'list',
                    'sub_fields' => [
                        [
                            'key' => 'field_nomination_list_item_text',
                            'label' => 'Текст',
                            'name' => 'text',
                            'type' => 'qtranslate_textarea',
                            'new_lines' => 'br',
                            'rows' => 2,
                        ],
                    ],
                ],
            ],
        ],
    ],
    'location' => acf_get_template_location_array('teleprojects')
]);
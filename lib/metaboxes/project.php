<?php
acf_add_local_field_group([
    'key' => 'group_project_hero',
    'title' => 'Шапка',
    'fields' => [
        [
            'key' => 'field_project_hero_slide_image',
            'label' => 'Изображение',
            'name' => 'project-hero__image',
            'type' => 'image',
        ],
        [
            'key' => 'field_project_hero_slide_badge',
            'label' => 'Лейбл',
            'name' => 'project-hero__badge',
            'type' => 'qtranslate_text',
        ],
        [
            'key' => 'field_project_hero_slide_title',
            'label' => 'Заголовок',
            'name' => 'project-hero__title',
            'type' => 'qtranslate_textarea',
            'new_lines' => 'br',
            'rows' => 2,
        ],
        [
            'key' => 'field_project_hero_slide_button_text',
            'label' => 'Текст кнопки',
            'name' => 'project-hero__button-text',
            'type' => 'qtranslate_text',
        ],
    ],
    'location' => [
        [
            [
                'param' => 'post_type',
                'operator' => '==',
                'value' => "project",
            ],
        ],
    ]
]);

acf_add_local_field_group([
    'key' => 'group_about_project',
    'title' => 'О проекте',
    'fields' => [
        [
            'key' => 'field_about_project_title',
            'label' => 'Заголовок',
            'name' => 'about-project__title',
            'type' => 'qtranslate_text',
        ],
        [
            'key' => 'field_about_project_subtitle',
            'label' => 'Пдзаголовок',
            'name' => 'about-project__subtitle',
            'type' => 'qtranslate_text',
        ],
        [
            'key' => 'field_about_project_text',
            'label' => 'Текст',
            'name' => 'about-project__text',
            'type' => 'qtranslate_wysiwyg',
            'default_value' => '',
            'tabs' => 'all',
            'toolbar' => 'basic',
            'media_upload' => 0,
            'delay' => 0,
        ],
    ],
    'location' => [
        [
            [
                'param' => 'post_type',
                'operator' => '==',
                'value' => "project",
            ],
        ],
    ]
]);

/* Project category */
acf_add_local_field_group([
    'key' => 'group_project_category',
    'title' => 'Общие настройки',
    'fields' => [
        [
            'key' => 'field_project_category_bg',
            'label' => 'Фоновое изображение',
            'name' => 'project-category__bg',
            'type' => 'image',
        ],
        [
            'key' => 'field_project_category_post_template',
            'label' => 'Шаблон карточки',
            'name' => 'project-category__post-template',
            'type' => 'select',
            'choices' => [
                'plain' => 'Изображение + текст',
                'hover' => 'Текст по наведению'
            ]
        ],
    ],
    'location' => [
        [
            [
                'param' => 'taxonomy',
                'operator' => '==',
                'value' => 'project-category',
            ],
        ],
    ]
]);
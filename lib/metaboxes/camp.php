<?php
acf_add_local_field_group([
    'key' => 'group_camp_head',
    'title' => 'Шапка',
    'fields' => [
        [
            'key' => 'field_camp_head_image',
            'label' => 'Фоновое изображение',
            'type' => 'image',
            'name' => 'camp-head__image',
        ],
        [
            'key' => 'field_camp_head_title',
            'label' => 'Заголовок',
            'name' => 'camp-head__title',
            'type' => 'qtranslate_textarea',
            'new_lines' => 'br',
            'rows' => 1,
        ],
        [
            'key' => 'field_camp_head_list',
            'label' => 'Список',
            'type' => 'repeater',
            'layout' => 'block',
            'name' => 'camp-head__list',
            'sub_fields' => [
                [
                    'key' => 'field_camp_head_list_item_title',
                    'label' => 'Заголовок',
                    'name' => 'title',
                    'type' => 'qtranslate_text',
                ],
                [
                    'key' => 'field_camp_head_list_item_text',
                    'label' => 'Текст',
                    'name' => 'text',
                    'type' => 'qtranslate_text',
                ],
            ],
        ],
        [
            'key' => 'field_camp_head_link_text',
            'label' => 'Текст ссылки',
            'name' => 'camp-head__link-text',
            'type' => 'qtranslate_text',
        ],
        [
            'key' => 'field_camp_head_link_url',
            'label' => 'URL ссылки',
            'name' => 'camp-head__link-URL',
            'type' => 'qtranslate_text',
        ],
        [
            'key' => 'field_camp_head_button_text',
            'label' => 'Текст кнопки',
            'name' => 'camp-head__button-text',
            'type' => 'qtranslate_text',
        ],
        [
            'key' => 'field_camp_head_button_href',
            'label' => 'URL кнопки',
            'name' => 'camp-head__button-href',
            'type' => 'qtranslate_text',
        ],
    ],
    'location' => acf_get_template_location_array('camp')
]);

acf_add_local_field_group([
    'key' => 'group_about_camp',
    'title' => 'О лагере',
    'fields' => [
        [
            'key' => 'field_about_camp_title',
            'label' => 'Заголовок',
            'name' => 'about-camp__title',
            'type' => 'qtranslate_text',
        ],
        [
            'key' => 'field_about_camp_subtitle',
            'label' => 'Подзаголовок',
            'name' => 'about-camp__subtitle',
            'type' => 'qtranslate_text',
        ],
        [
            'key' => 'field_about_camp_text',
            'label' => 'Текст',
            'name' => 'about-camp__text',
            'type' => 'qtranslate_wysiwyg',
            'default_value' => '',
            'tabs' => 'all',
            'toolbar' => 'basic',
            'media_upload' => 0,
            'delay' => 0,
        ],
        [
            'key' => 'field_about_camp_list',
            'label' => 'Список',
            'type' => 'repeater',
            'layout' => 'block',
            'name' => 'about-camp__list',
            'sub_fields' => [
                [
                    'key' => 'field_about_camp_list_item_icon',
                    'label' => 'Иконка',
                    'name' => 'icon',
                    'type' => 'image',
                ],
                [
                    'key' => 'field_about_camp_list_item_link',
                    'label' => 'Ссылка',
                    'name' => 'link',
                    'type' => 'file',
                ],
                [
                    'key' => 'field_about_camp_list_item_text',
                    'label' => 'Текст',
                    'name' => 'text',
                    'type' => 'qtranslate_textarea',
                    'new_lines' => 'br',
                    'rows' => 2,
                ],
            ]
        ],
    ],
    'location' => acf_get_template_location_array('camp')
]);

acf_add_local_field_group([
    'key' => 'group_shifts',
    'title' => 'Смены',
    'fields' => [
        [
            'key' => 'field_shifts_title',
            'label' => 'Заголовок',
            'name' => 'shifts__title',
            'type' => 'qtranslate_text',
        ],
        [
            'key' => 'field_shifts',
            'label' => 'Смены',
            'type' => 'repeater',
            'layout' => 'block',
            'name' => 'shifts',
            'sub_fields' => [
                [
                    'key' => 'field_shift_icon',
                    'label' => 'Иконка',
                    'name' => 'icon',
                    'type' => 'image',
                ],
                [
                    'key' => 'field_shift_name',
                    'label' => 'Название',
                    'name' => 'name',
                    'type' => 'qtranslate_text',
                ],
                [
                    'key' => 'field_shift_dates',
                    'label' => 'Даты',
                    'name' => 'dates',
                    'type' => 'qtranslate_text',
                ],
                [
                    'key' => 'field_shift_dates_description',
                    'label' => 'Описание',
                    'name' => 'text',
                    'type' => 'qtranslate_textarea',
                    'new_lines' => 'br',
                ],
                [
                    'key' => 'field_shift_link',
                    'label' => 'Ссылка',
                    'name' => 'link',
                    'type' => 'qtranslate_text',
                ],
                [
                    'key' => 'field_shift_dates_price',
                    'label' => 'Цена',
                    'name' => 'price',
                    'type' => 'qtranslate_text',
                ],
                [
                    'key' => 'field_shift_dates_price_striken',
                    'label' => 'Перечеркнутая цена ',
                    'name' => 'price-striken',
                    'type' => 'qtranslate_text',
                ],
                [
                    'key' => 'field_shift_dates_footnote',
                    'label' => 'Сноска ',
                    'name' => 'footnote',
                    'type' => 'qtranslate_text',
                ],
                [
                    'key' => 'field_shift_schedule_title',
                    'label' => 'Распорядок дня - заголовок',
                    'name' => 'schedule__title',
                    'type' => 'qtranslate_text',
                ],
                [
                    'key' => 'field_shift_schedule',
                    'label' => 'Распорядок дня - карточки',
                    'type' => 'repeater',
                    'layout' => 'block',
                    'name' => 'schedule',
                    'sub_fields' => [
                        [
                            'key' => 'field_shift_schedule_item_image',
                            'label' => 'Изображение',
                            'name' => 'image',
                            'type' => 'image',
                        ],
                        [
                            'key' => 'field_shift_schedule_item_title',
                            'label' => 'Заголовок',
                            'name' => 'title',
                            'type' => 'qtranslate_text',
                        ],
                        [
                            'key' => 'field_shift_schedule_item_text',
                            'label' => 'Текст',
                            'name' => 'text',
                            'type' => 'qtranslate_textarea',
                            'new_lines' => 'br',
                        ],
                    ]
                ],                
            ]
        ],
    ],
    'location' => acf_get_template_location_array('camp')
]);

acf_add_local_field_group([
    'key' => 'group_features',
    'title' => 'Особенности лагеря',
    'fields' => [
        [
            'key' => 'field_features_title',
            'label' => 'Заголовок',
            'name' => 'features__title',
            'type' => 'qtranslate_text',
        ],
        [
            'key' => 'field_features',
            'label' => 'Карточки',
            'type' => 'repeater',
            'layout' => 'block',
            'name' => 'features',
            'sub_fields' => [
                [
                    'key' => 'field_features_item_image',
                    'label' => 'Изображение',
                    'name' => 'image',
                    'type' => 'image',
                ],
                [
                    'key' => 'field_features_item_title',
                    'label' => 'Заголовок',
                    'name' => 'title',
                    'type' => 'qtranslate_text',
                ],
                [
                    'key' => 'field_features_item_text',
                    'label' => 'Текст',
                    'name' => 'text',
                    'type' => 'qtranslate_textarea',
                    'rows' => 2
                ],
                [
                    'key' => 'field_features_item_list',
                    'label' => 'Список',
                    'type' => 'repeater',
                    'layout' => 'block',
                    'name' => 'list',
                    'sub_fields' => [
                        [
                            'key' => 'field_features_item_list_text',
                            'label' => 'Текст',
                            'name' => 'text',
                            'type' => 'qtranslate_textarea',
                            'new_lines' => 'br',
                            'rows' => 2
                        ],
                    ]
                ],
            ]
        ],
    ],
    'location' => acf_get_template_location_array('camp')
]);

acf_add_local_field_group([
    'key' => 'group_conditions',
    'title' => 'Наши условия',
    'fields' => [
        [
            'key' => 'field_conditions_title',
            'label' => 'Заголовок',
            'name' => 'conditions__title',
            'type' => 'qtranslate_text',
        ],
        [
            'key' => 'field_conditions_button_text',
            'label' => 'Текст кнопки',
            'name' => 'conditions__button-text',
            'type' => 'qtranslate_text',
        ],
        [
            'key' => 'field_conditions_button_href',
            'label' => 'Ссылка кнопки (файл)',
            'name' => 'conditions__button-href',
            'type' => 'file',
        ],
        [
            'key' => 'field_conditions_tabs',
            'label' => 'Вкладки',
            'type' => 'repeater',
            'layout' => 'block',
            'name' => 'conditions__tabs',
            'sub_fields' => [
                [
                    'key' => 'field_conditions_tab_item_icon',
                    'label' => 'Иконка',
                    'name' => 'icon',
                    'type' => 'image',
                ],
                [
                    'key' => 'field_conditions_tab_item_name',
                    'label' => 'Название',
                    'name' => 'name',
                    'type' => 'qtranslate_text',
                ],
                [
                    'key' => 'field_conditions_tab_item_template',
                    'label' => 'Шаблон',
                    'name' => 'template',
                    'type' => 'select',
                    'choices' => [
                        'accomodation' => 'Проживание',
                        'food'         => 'Питание',
                        'leisure'      => 'Досуг',
                    ]
                ],
                [
                    'key' => 'field_conditions_tab_item_image',
                    'label' => 'Изображение',
                    'name' => 'image',
                    'type' => 'image',
                ],
                [
                    'key' => 'field_conditions_tab_item_text',
                    'label' => 'Текст',
                    'name' => 'text',
                    'type' => 'qtranslate_textarea',
                    'new_lines' => 'br',
                    'rows' => 2,
                    'conditional_logic' => [
                        [
                            [
                                'field' => 'field_conditions_tab_item_template',
                                'operator' => '==',
                                'value' => 'food',
                            ],
                        ],
                    ],
                ],
                [
                    'key' => 'field_conditions_tab_item_list',
                    'label' => 'Список',
                    'type' => 'repeater',
                    'layout' => 'block',
                    'name' => 'list',
                    'sub_fields' => [
                        [
                            'key' => 'field_conditions_tab_item_list_text',
                            'label' => 'Текст',
                            'name' => 'text',
                            'type' => 'qtranslate_textarea',
                            'new_lines' => 'br',
                            'rows' => 2
                        ],
                    ],
                    'conditional_logic' => [
                        [
                            [
                                'field' => 'field_conditions_tab_item_template',
                                'operator' => '==',
                                'value' => 'food',
                            ],
                        ],
                        [
                            [
                                'field' => 'field_conditions_tab_item_template',
                                'operator' => '==',
                                'value' => 'leisure',
                            ],
                        ],
                    ],
                ],
                [
                    'key' => 'field_conditions_tab_item_list2-title',
                    'label' => 'Заголовок списка (левая колонка)',
                    'name' => 'list2-title',
                    'type' => 'qtranslate_text',
                    'conditional_logic' => [
                        [
                            [
                                'field' => 'field_conditions_tab_item_template',
                                'operator' => '==',
                                'value' => 'accomodation',
                            ],
                        ],
                    ],
                ],
                [
                    'key' => 'field_conditions_tab_item_list2',
                    'label' => 'Список (левая колонка)',
                    'type' => 'repeater',
                    'layout' => 'block',
                    'name' => 'list2',
                    'sub_fields' => [
                        [
                            'key' => 'field_conditions_tab_item_list2_text',
                            'label' => 'Текст',
                            'name' => 'text',
                            'type' => 'qtranslate_textarea',
                            'new_lines' => 'br',
                            'rows' => 2
                        ],
                    ],
                    'conditional_logic' => [
                        [
                            [
                                'field' => 'field_conditions_tab_item_template',
                                'operator' => '==',
                                'value' => 'accomodation',
                            ],
                        ],
                    ],
                ],
                [
                    'key' => 'field_conditions_tab_item_list3-title',
                    'label' => 'Заголовок списка (правая колонка)',
                    'name' => 'list3-title',
                    'type' => 'qtranslate_text',
                    'conditional_logic' => [
                        [
                            [
                                'field' => 'field_conditions_tab_item_template',
                                'operator' => '==',
                                'value' => 'accomodation',
                            ],
                        ],
                    ],
                ],
                [
                    'key' => 'field_conditions_tab_item_list3',
                    'label' => 'Список (правая колонка)',
                    'type' => 'repeater',
                    'layout' => 'block',
                    'name' => 'list3',
                    'sub_fields' => [
                        [
                            'key' => 'field_conditions_tab_item_list3_text',
                            'label' => 'Текст',
                            'name' => 'text',
                            'type' => 'qtranslate_textarea',
                            'new_lines' => 'br',
                            'rows' => 2
                        ],
                    ],
                    'conditional_logic' => [
                        [
                            [
                                'field' => 'field_conditions_tab_item_template',
                                'operator' => '==',
                                'value' => 'accomodation',
                            ],
                        ],
                    ],
                ],
            ]
        ],
    ],
    'location' => acf_get_template_location_array('camp')
]);

acf_add_local_field_group([
    'key' => 'group_camp_crew',
    'title' => 'Преподаватели/артисты',
    'fields' => [
        [
            'key' => 'field_crew_hide',
            'label' => 'Отображение блока',
            'type' => 'radio',
            'name' => 'crew__display',
            'choices' => [
                'show' => 'Показывать',
                'hide' => 'Скрывать'
            ]
        ],
    ],
    'location' => acf_get_template_location_array('camp')
]);
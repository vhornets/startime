<?php
acf_add_local_field_group([
    'key' => 'group_crew_member',
    'title' => 'Общие настройки',
    'fields' => [
        [
            'key' => 'field_crew_description_short',
            'label' => 'Короткое описание',
            'name' => 'description-short',
            'type' => 'qtranslate_textarea',
            'new_lines' => 'br',
            'rows' => 2,
        ],
    ],
    'location' => [
        [
            [
                'param' => 'post_type',
                'operator' => '==',
                'value' => "crew",
            ],
        ],
    ]
]);
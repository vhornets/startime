<?php
acf_add_local_field_group([
    'key' => 'group_home_hero',
    'title' => 'Шапка',
    'fields' => [
        [
            'key' => 'field_hero_slides',
            'label' => 'Слайды',
            'type' => 'repeater',
            'layout' => 'block',
            'name' => 'hero__slides',
            'sub_fields' => [
                [
                    'key' => 'field_hero_slide_image',
                    'label' => 'Изображение',
                    'name' => 'image',
                    'type' => 'image',
                ],
                [
                    'key' => 'field_hero_slide_title',
                    'label' => 'Заголовок',
                    'name' => 'title',
                    'type' => 'qtranslate_textarea',
                    'new_lines' => 'br',
                    'rows' => 2,
                ],
                [
                    'key' => 'field_hero_slide_countdown',
                    'label' => 'Счетчик',
                    'name' => 'countdown',
                    'type' => 'date_picker',
                    'display_format' => 'm/d/Y',
                    'return_format' => 'm/d/Y',
                ],
                [
                    'key' => 'field_hero_slide_button_text',
                    'label' => 'Текст кнопки',
                    'name' => 'button-text',
                    'type' => 'qtranslate_text',
                ],
                [
                    'key' => 'field_hero_slide_button_href',
                    'label' => 'Ссылка кнопки',
                    'name' => 'button-href',
                    'type' => 'qtranslate_text',
                ],
            ],
        ]
    ],
    'location' => acf_get_template_location_array('home')
]);

acf_add_local_field_group([
    'key' => 'group_home_lessons',
    'title' => 'Направления',
    'fields' => [
        [
            'key' => 'field_lessons_title',
            'label' => 'Заголовок',
            'name' => 'lessons__title',
            'type' => 'qtranslate_text',
        ],
    ],
    'location' => acf_get_template_location_array('home')
]);

acf_add_local_field_group([
    'key' => 'group_about',
    'title' => 'О продюссерском центре',
    'fields' => [
        [
            'key' => 'field_about_image',
            'label' => 'Изображение',
            'name' => 'about__image',
            'type' => 'image',
        ],
        [
            'key' => 'field_about_title',
            'label' => 'Заголовок',
            'name' => 'about__title',
            'type' => 'qtranslate_text',
        ],
        [
            'key' => 'field_about_text',
            'label' => 'Текст',
            'name' => 'about__text',
            'type' => 'qtranslate_textarea',
            'rows' => 2,
        ],
        [
            'key' => 'field_about_title2',
            'label' => 'Заголовок 2',
            'name' => 'about__title2',
            'type' => 'qtranslate_text',
        ],
        [
            'key' => 'field_about_text2',
            'label' => 'Текст 2',
            'name' => 'about__text2',
            'type' => 'qtranslate_textarea',
        ],
        [
            'key' => 'field_about_button_text',
            'label' => 'Текст кнопки',
            'name' => 'about__button-text',
            'type' => 'qtranslate_text',
        ],
        [
            'key' => 'field_about_button_href',
            'label' => 'Ссылка кнопки',
            'name' => 'about__button-href',
            'type' => 'qtranslate_text',
        ],
    ],
    'location' => acf_get_template_location_array('home')
]);

acf_add_local_field_group([
    'key' => 'group_feedback',
    'title' => 'Отзывы',
    'fields' => [
        [
            'key' => 'field_feedback_title',
            'label' => 'Заголовок',
            'name' => 'feedback__title',
            'type' => 'qtranslate_text',
        ]
    ],
    'location' => acf_get_template_location_array('home')
]);
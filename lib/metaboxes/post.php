<?php
acf_add_local_field_group([
    'key' => 'group_post_hero',
    'title' => 'Шапка',
    'fields' => [
        [
            'key' => 'field_post_hero_slide_image',
            'label' => 'Изображение',
            'name' => 'post-hero__image',
            'type' => 'image',
        ],
        [
            'key' => 'field_post_hero_slide_title',
            'label' => 'Заголовок',
            'name' => 'post-hero__title',
            'type' => 'qtranslate_textarea',
            'new_lines' => 'br',
            'rows' => 1,
        ],
        [
            'key' => 'field_post_hero_slide_subtitle',
            'label' => 'Подзаголовок',
            'name' => 'post-hero__subtitle',
            'type' => 'qtranslate_textarea',
            'new_lines' => 'br',
            'rows' => 1,
        ],
    ],
    'location' => [
        [
            [
                'param' => 'post_type',
                'operator' => '==',
                'value' => "post",
            ],
        ],
    ]
]);

acf_add_local_field_group([
    'key' => 'group_post',
    'title' => 'Общие настройки',
    'fields' => [
        [
            'key' => 'field_post_description',
            'label' => 'Короткое описание',
            'name' => 'post__description',
            'type' => 'qtranslate_textarea',
            'new_lines' => 'br',
            'rows' => 3,
        ],
    ],
    'location' => [
        [
            [
                'param' => 'post_type',
                'operator' => '==',
                'value' => "post",
            ],
        ],
    ]
]);
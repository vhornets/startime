<?php
acf_add_local_field_group([
    'key' => 'group_faq_head',
    'title' => 'Шапка',
    'fields' => [
        [
            'key' => 'field_faq_head_image',
            'label' => 'Изображение',
            'name' => 'faq-head__image',
            'type' => 'image',
        ],
        [
            'key' => 'field_faq_head_title',
            'label' => 'Заголовок',
            'name' => 'faq-head__title',
            'type' => 'qtranslate_textarea',
            'new_lines' => 'br',
            'rows' => 1,
        ],
        [
            'key' => 'field_faq_head_button_text',
            'label' => 'Текст кнопки',
            'name' => 'faq-head__button-text',
            'type' => 'qtranslate_text',
        ],
    ],
    'location' => acf_get_template_location_array(['faq', 'vacancies'])
]);

acf_add_local_field_group([
    'key' => 'group_faq_questions',
    'title' => 'Вопрос/ответ',
    'fields' => [
        [
            'key' => 'field_faq_questions',
            'label' => 'Вопрос/ответ',
            'type' => 'repeater',
            'layout' => 'block',
            'name' => 'faq__questions',
            'sub_fields' => [
                [
                    'key' => 'field_faq_item_question',
                    'label' => 'Вопрос',
                    'name' => 'question',
                    'type' => 'qtranslate_textarea',
                    'new_lines' => 'br',
                    'rows' => 2,
                ],
                [
                    'key' => 'field_faq_item_list_title',
                    'label' => 'Заголовок списка',
                    'name' => 'list-title',
                    'type' => 'qtranslate_text',
                ],
                [
                    'key' => 'field_faq_item_list',
                    'label' => 'Список',
                    'type' => 'repeater',
                    'name' => 'list',
                    'sub_fields' => [
                        [
                            'key' => 'field_faq_item_list_item_text',
                            'label' => 'Текст',
                            'name' => 'text',
                            'type' => 'qtranslate_textarea',
                            'new_lines' => 'br',
                            'rows' => 1,
                        ],
                    ],
                ]
            ],
        ]
    ],
    'location' => acf_get_template_location_array('faq')
]);
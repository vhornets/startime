<?php
function vh_setup_options_page() {
    acf_add_options_page(array(
        'page_title'    => 'Главное',
        'menu_title'    => 'Настройки темы',
        'menu_slug'     => 'theme-general-settings',
        'capability'    => 'edit_posts',
        'redirect'      => false
    ));

    acf_add_options_sub_page(array(
        'page_title'    => 'Контакты',
        'menu_title'    => 'Контакты',
        'menu_slug'     => 'theme-contacts-settings',
        'parent_slug'   => 'theme-general-settings',
    ));

    acf_add_options_sub_page(array(
        'page_title'    => 'Страницы',
        'menu_title'    => 'Страницы',
        'menu_slug'     => 'theme-pages-settings',
        'parent_slug'   => 'theme-general-settings',
    ));

    acf_add_local_field_group(array (
        'key' => 'group_settings',
        'title' => 'Настройки',
        'fields' => array (
            array (
                'key' => 'field_settings_logo',
                'label' => 'Лого',
                'name' => 'logo',
                'type' => 'file',
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'options_page',
                    'operator' => '==',
                    'value' => 'theme-general-settings',
                ),
            ),
        ),
    ));

    // КОНТАКТЫ
    acf_add_local_field_group(array (
        'key' => 'group_settings_contacts',
        'title' => 'Контакты',
        'fields' => array (
            array (
                'key' => 'field_settings_facebook',
                'label' => 'Facebook',
                'name' => 'facebook_link',
                'type' => 'text',
            ),
            array (
                'key' => 'field_settings_instagram',
                'label' => 'Instagram',
                'name' => 'instagram_link',
                'type' => 'text',
            ),
            array (
                'key' => 'field_settings_youtube',
                'label' => 'Youtube',
                'name' => 'youtube_link',
                'type' => 'text',
            ),
            array (
                'key' => 'field_settings_address',
                'label' => 'Адрес',
                'name' => 'address',
                'type' => 'qtranslate_text',
            ),
            array (
                'key' => 'field_settings_phone',
                'label' => 'Телефон',
                'name' => 'phone',
                'type' => 'text',
            ),
            array (
                'key' => 'field_settings_phones_additional',
                'label' => 'Дополнительные номера телефонов',
                'type' => 'repeater',
                'name' => 'phones-additional',
                'sub_fields' => [
                    [
                        'key' => 'field_phones_item_phone',
                        'label' => 'Номер',
                        'name' => 'phone',
                        'type' => 'text',
                    ],
                ],
            ),
            array (
                'key' => 'field_settings_emails',
                'label' => 'E-mail-адреса',
                'type' => 'repeater',
                'name' => 'emails',
                'layout' => 'table',
                'sub_fields' => [
                    [
                        'key' => 'field_emails_item_title',
                        'label' => 'Заголовок',
                        'name' => 'title',
                        'type' => 'qtranslate_text',
                    ],
                    [
                        'key' => 'field_emails_item_email',
                        'label' => 'Адрес',
                        'name' => 'email',
                        'type' => 'text',
                    ],
                ],
            ),
            array (
                'key' => 'field_settings_schedule',
                'label' => 'Рабочее время',
                'name' => 'schedule',
                'type' => 'qtranslate_textarea',
                'new_lines' => 'br',
                'rows' => 2,
            ),
            array (
                'key' => 'field_settings_map',
                'label' => 'Карта',
                'name' => 'map',
                'type' => 'google_map',
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'options_page',
                    'operator' => '==',
                    'value' => 'theme-contacts-settings',
                ),
            ),
        ),
    ));

    // СТРАНИЦЫ
    acf_add_local_field_group(array (
        'key' => 'group_settings_pages',
        'title' => 'Страницы',
        'fields' => array (
            array (
                'key' => 'field_settings_page_services',
                'label' => 'Страница услуг',
                'name' => 'page-services',
                'type' => 'select',
                'choices' => vh_get_all_pages(),
            ),
            array (
                'key' => 'field_settings_page_courses',
                'label' => 'Страница обучения',
                'name' => 'page-courses',
                'type' => 'select',
                'choices' => vh_get_all_pages(),
            ),
            array (
                'key' => 'field_settings_page_form_response',
                'label' => 'Страница благодарности поле отправки формы',
                'name' => 'page-form-response',
                'type' => 'select',
                'choices' => vh_get_all_pages(),
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'options_page',
                    'operator' => '==',
                    'value' => 'theme-pages-settings',
                ),
            ),
        ),
    ));
}

add_action('wp_loaded', 'vh_setup_options_page');
<?php
require_once __DIR__ . '/amocrm/amocrm.phar';

function vh_theme_assets() {
    wp_register_style( 'vh-google-fonts', 'https://fonts.googleapis.com/css?family=Rubik:300,300i,400,400i,500,500i,700,700i&display=swap&subset=cyrillic' );
    wp_enqueue_style( 'vh-styles', get_template_directory_uri() . '/dist/css/app.css', array('vh-google-fonts'), '0.0.1' );

    if(ENV === "development" || ENV === "test") {
        wp_enqueue_script( 'vh-scripts', get_template_directory_uri() . '/dist/js/bundle.js', array(), '0.0.1', true );
    }

    else {
        wp_enqueue_script( 'vh-scripts', get_template_directory_uri() . '/dist/js/bundle.min.js', array(), '0.0.1', true );
    }
}
add_action( 'wp_enqueue_scripts', 'vh_theme_assets' );

function startime_setup_languages(){
    load_theme_textdomain('startime', get_template_directory() . '/languages');
}
add_action('after_setup_theme', 'startime_setup_languages');

function disable_wp_emojicons() {
    remove_action( 'admin_print_styles', 'print_emoji_styles' );
    remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
    remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
    remove_action( 'wp_print_styles', 'print_emoji_styles' );
    remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
    remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
    remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );

    add_filter( 'tiny_mce_plugins', 'disable_emojicons_tinymce' );
    add_filter( 'emoji_svg_url', '__return_false' );
}
add_action( 'init', 'disable_wp_emojicons' );

function disable_emojicons_tinymce( $plugins ) {
    if ( is_array( $plugins ) ) {
        return array_diff( $plugins, array( 'wpemoji' ) );
    } else {
       return array();
    }
}

function sendRequest() {
    if ( !isset( $_POST['_wpnonce'] ) || !wp_verify_nonce($_POST['_wpnonce'], 'send_request') ) {
       die('Wrong nonce.');
    }

    $name = get_post_var('name');
    $tel = get_post_var('tel');
    $birthday = get_post_var('birthday');
    $message = get_post_var('message');
    $section = get_post_var('section');
    $page = get_bloginfo('url') . get_post_var('_wp_http_referer');

    $request_body = "Имя: $name <br> Телефон: $tel";

    if($section) {
        $request_body .= "<br>Информация о всплывающей форме: $section";
    }

    if($message) {
        $request_body .= "<br>Сообщение: $message";
    }

    $request_body .= "<br>Страница: $page";

    $request = wp_insert_post(array(
        'post_title'   => $name,
        'post_content'   => $request_body,
        'post_type'    => 'request',
    ));

    if($request) {
        _e('Ваша заявка была успешно отправлена.', 'startime');

        $request_link = get_edit_post_link($request);

        sendmail('Новая заявка', "<a href='$request_link'>$request_link</a><br>" . $request_body);

        if(get_post_var("amocrm") && get_post_var("amocrm") == 1) {
            try {
                // Создание клиента
                //$amo = new \AmoCRM\Client('startimepc', 'prod@startime.ua', '450baacd2f2d6255575d63dbe3e285c433813de9');
                $amo = new \AmoCRM\Client('startimepc', 'nikulina@startime.ua', '8d48c7887aa2448542e4e8da78aee6d229734ba0');

                $responce['foundContacts'] = $amo->contact->apiList([
                    'query' => $tel,
                ]);
                $responce['timings']['contactsQueried'] = time();

                // Добавление и обновление контактов
                // Метод позволяет добавлять контакты по одному или пакетно,
                // а также обновлять данные по уже существующим контактам.
                $contact = $amo->contact;
                //$contact->debug(true); // Режим отладки
                $contact['name'] = $name;
                $contact['request_id'] = $_POST['_wpnonce'].rand(0, 9999).'-'.$name;
                $contact['date_create'] = $responce['now'];
                $contact['last_modified'] = $responce['now'];
                $contact['responsible_user_id'] = 898465;
                if ($tel) {
                    $contact->addCustomField(150621, [
                        [$tel, 'MOB'],
                    ]);
                }
  
                if ($birthday) {
                    $contact->addCustomField(156291, [
                        [strtotime($birthday)],
                    ]);
                }

                // add contact now
                if (empty($responce['foundContacts'])) {
                    $responce['amoApiSentDataContact'] = $contact;
                    $id = $contact->apiAdd();
                    $responce['amoDataContact'] = $id;
                    $responce['timings']['contactAdded'] = time();
                }

                $ind = 16844251;

                // create new lead
                $lead = $amo->lead;
                // $lead->debug(true); // Режим отладки
                $lead['name'] = 'Новая сделка на сайте startime.ua';
                $lead['date_create'] = $responce['now'];
                $lead['status_id'] = $ind;
                // $lead['price'] = 3000;
                $lead['responsible_user_id'] = $contact['responsible_user_id'];
                $lead['tags'] = ['Сделка на сайте'];
                $lead->addCustomField(461425, [
                    [1000625, 'Сайт'],
                ]);
                $lead->addCustomField(448351, [
                    [$page],
                ]);
                $responce['amoApiSentDataLead']=$lead;
                $leadid = $lead->apiAdd();
                $responce['amoDataLead']=$leadid;
                $responce['timings']['leadCreated']=time();


                $link = $amo->links;
                $link['from'] = 'leads';
                $link['from_id'] = $leadid;
                $link['to'] = 'contacts';
                $link['to_id'] = (!empty($responce['amoDataContact']))?$responce['amoDataContact']:$responce['foundContacts'][0]['id'];
                $linkid=$link->apiLink();
                $responce['amoDataLink']=$linkid;
                $responce['timings']['leadLinked']=time();

            } catch (\AmoCRM\Exception $e) {
                printf('Error (%d): %s' . PHP_EOL, $e->getCode(), $e->getMessage());
            }            
        }
    }

    else {
        _e('Произошла ошибка. Пожалуйста, попробуйте позже.', 'startime');
    }

    die();
}
add_action('wp_ajax_sendRequest', 'sendRequest');
add_action('wp_ajax_nopriv_sendRequest', 'sendRequest');

function sendVote() {
    if ( !isset( $_POST['_wpnonce'] ) || !wp_verify_nonce($_POST['_wpnonce'], 'send_vote') ) {
       die('Wrong nonce.');
    }

    $nomineeId = get_post_var('nominee-id');
    $page = get_post_var('_wp_http_referer');
    $votes = (int) get_field('votes', $nomineeId);

    update_field('votes', ++$votes, $nomineeId);

    setcookie('nomineeVote', $nomineeId, time()+3600*24*30, '/');

    echo $votes;

    die();
}
add_action('wp_ajax_sendVote', 'sendVote');
add_action('wp_ajax_nopriv_sendVote', 'sendVote');

function show_all_projects_in_archive( $query ) {
    if ( ! is_admin() && $query->is_main_query() ) {
        if ( is_tax('project-category') ) {
            $query->set( 'posts_per_page', -1 );
        }
    }
}
add_action( 'pre_get_posts', 'show_all_projects_in_archive' );
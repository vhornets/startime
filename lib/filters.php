<?php
function my_acf_google_map_api( $api ){
    $api['key'] = 'AIzaSyCadoAMTROrTEgmNnBOJFO9N-Ld3EwJS6Q';

    return $api;
}
add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');

add_filter('wp_mail_content_type', 'mail_content_type_custom');
function mail_content_type_custom() { return 'text/html'; }

add_filter('embed_oembed_html', 'vh_flex_video');
function vh_flex_video( $code ){
    if(stripos($code, 'iframe') !== FALSE) {
        $code = '<div class="o-video-embed">'.$code.'</div>';
    }

    return $code;
}

add_filter( 'post_gallery', 'vh_post_gallery', 10, 2 );
function vh_post_gallery( $output, $attr ) {

    // Initialize
    global $post, $wp_locale;

    // Gallery instance counter
    static $instance = 0;
    $instance++;

    // Validate the author's orderby attribute
    if ( isset( $attr['orderby'] ) ) {
        $attr['orderby'] = sanitize_sql_orderby( $attr['orderby'] );
        if ( ! $attr['orderby'] ) unset( $attr['orderby'] );
    }

    // Get attributes from shortcode
    extract( shortcode_atts( array(
        'order'      => 'ASC',
        'orderby'    => 'menu_order ID',
        'id'         => $post->ID,
        'itemtag'    => 'dl',
        'icontag'    => 'dt',
        'captiontag' => 'dd',
        'columns'    => 3,
        'size'       => 'large',
        'include'    => '',
        'exclude'    => ''
    ), $attr ) );

    // Initialize
    $id = intval( $id );
    $attachments = array();
    if ( $order == 'RAND' ) $orderby = 'none';

    if ( ! empty( $include ) ) {

        // Include attribute is present
        $include = preg_replace( '/[^0-9,]+/', '', $include );
        $_attachments = get_posts( array( 'include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby ) );

        // Setup attachments array
        foreach ( $_attachments as $key => $val ) {
            $attachments[ $val->ID ] = $_attachments[ $key ];
        }

    } else if ( ! empty( $exclude ) ) {

        // Exclude attribute is present
        $exclude = preg_replace( '/[^0-9,]+/', '', $exclude );

        // Setup attachments array
        $attachments = get_children( array( 'post_parent' => $id, 'exclude' => $exclude, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby ) );
    } else {
        // Setup attachments array
        $attachments = get_children( array( 'post_parent' => $id, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby ) );
    }

    if ( empty( $attachments ) ) return '';

    // Filter gallery differently for feeds
    if ( is_feed() ) {
        $output = "\n";
        foreach ( $attachments as $att_id => $attachment ) $output .= wp_get_attachment_link( $att_id, $size, true ) . "\n";
        return $output;
    }

    // Filter tags and attributes
    $itemtag = tag_escape( $itemtag );
    $captiontag = tag_escape( $captiontag );
    $columns = intval( $columns );
    $itemwidth = $columns > 0 ? floor( 100 / $columns ) : 100;
    $float = is_rtl() ? 'right' : 'left';
    $selector = "gallery-{$instance}";

    // Filter gallery CSS
    $output = "<div id='$selector' class='l-gallery l-gallery--$columns c-gallery galleryid-{$id} js-gallery'>";

    // Iterate through the attachments in this gallery instance
    $i = 0;
    foreach ( $attachments as $id => $attachment ) {
        $gallery_item_src = wp_get_attachment_image_src($id, 'large');
        $gallery_item_src = $gallery_item_src[0];

        // Attachment link
        $link = isset( $attr['link'] ) && 'file' == $attr['link'] ? wp_get_attachment_link( $id, $size, false, false ) : wp_get_attachment_link( $id, $size, true, false );

        // Start itemtag
        $output .= "<{$itemtag} class='l-gallery__item'>";

        $output .= "<a href='$gallery_item_src' class='c-gallery__link js-gallery-link'>";
        $output .= "<div class='c-gallery__image' style='background-image: url($gallery_item_src);'>";
        $output .= "</div>";
        $output .= "</a>";

        $output .= "</{$itemtag}>";
    }

    // End gallery output
    $output .= "</div>\n";

    return $output;
}

if(ENV == 'development' || ENV == 'test' || ENV == 'production') {
    add_filter('show_admin_bar', '__return_false');
}

function posts_link_attributes() {
    return 'class="o-button-default js-load-next-posts-page" data-loading-text="' . __('Загрузка...', 'startime') . '"';
}
add_filter('next_posts_link_attributes', 'posts_link_attributes');
add_filter('previous_posts_link_attributes', 'posts_link_attributes');

add_filter ('get_archives_link',
function ($link_html, $url, $text, $format, $before, $after) {
    $classes = '';

    if(is_year()) {
        $classes .= ' is-active';
    }

    $link_html = "<li class='CAPS source-bold'><a href='$url' class='$classes'>"
               . "$text"
               . '</a></li>';

    return $link_html;
}, 10, 6);
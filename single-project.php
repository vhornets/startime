<?php get_header(); ?>

<section class="c-section-hero">
    <div class="c-slider-hero__images">
        <img src="<?php echo get_field('project-hero__image')['url']; ?>" alt="" class="c-slider-hero__image">
    </div>

    <div class="l-container">
        <div class="c-slider-hero">
            <div class="c-slider-hero__slide">
                <div class="o-badge c-slider-hero__badge"><?php the_field('project-hero__badge'); ?></div>

                <h1 class="c-slider-hero__title">
                    <?php the_field('project-hero__title'); ?>
                </h1>

                <a href="#" class="o-button-default c-slider-hero__button open-popup" data-open="entry" data-section="<?php echo strip_tags(get_field('project-hero__title')); ?>">
                    <?php the_field('project-hero__button-text'); ?>
                </a>
            </div>
        </div>
    </div>
</section>

<section class="c-section-about-course">
    <img src="<?php bloginfo('template_url'); ?>/img/templates/teleprojects/about_bg.svg" class="c-section-about-course__bg" />

    <div class="l-about-course">
        <div class="c-about-camp">
            <h2 class="c-about-camp__title">
                <?php the_field('about-project__title'); ?>
            </h2>

            <h5 class="c-about-camp__title2">
                <?php the_field('about-project__subtitle'); ?>
            </h5>

            <div class="c-about-camp__text">
                <?php echo apply_filters( 'the_content', get_field('about-project__text') ); ?>
            </div>
        </div>
    </div>
</section>

<section class="c-section-teachers">
    <?php if(get_field('crew__display') != 'hide'): ?>
        <img src="<?php bloginfo('template_url'); ?>/img/teachers-bg-pattern.svg" class="c-section-teachers__bg" />

        <?php get_template_part('partials/crew-single-category'); ?>
    <?php endif; ?>
</section>

<section class="c-section-photos">
    <?php get_template_part('partials/gallery'); ?>
</section>

<section class="c-section-video">
    <?php get_template_part('partials/video'); ?>
</section>

<section class="c-section-form">
    <img src="<?php bloginfo('template_url'); ?>/img/form-bg-pattern.svg" class="c-section-form__bg" />

    <?php get_template_part('partials/form-horizontal'); ?>
</section>

<?php get_footer(); ?>

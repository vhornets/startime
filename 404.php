<?php get_header(); ?>

<?php get_template_part('partials/breadcrumbs'); ?>

<section class="404-section" style="padding-top:130px; margin-top:0;">
    <div class="l-container">
        <h1 class="text-center">
            404
        </h1>

      <p>Не переживайте, скорее всего Вы ошиблись в адресе страницы. По этим ссылкам вы попадёте куда нужно:</p>    
      <p><a href="<?=get_home_url(); ?>">На главную</a></p>

      <p>Эстрадная школа для <a href="https://startime.ua/kursi/childrens">детей</a> и  <a href="https://startime.ua/kursi/vzroslie">взрослых</a>и <a href="https://startime.ua/kursi/individual">индивидуальная</a>.</p>
      <p>Детский летний <a href="/camp">лагерь</a>.</p>
      <p>Наши <a href="">проекты</a>.</p>
      
    </div>
</section>

<?php get_footer(); ?>

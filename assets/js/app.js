import './jquery-global.js';
import 'remodal';
import 'jquery-countdown';
import Plyr from 'plyr';

import Utils from './utils';

import Accordion from './components/accordion';
import Blog from './components/blog';
import Forms from './components/forms';
import GoogleMap from './components/google-map';
import MainMenu from './components/main-menu';
import Sliders from './components/sliders';
import Tabs from './components/tabs';
import Video from './components/video';
import Popup from './components/popup'

export default class App {
    constructor() {
        new Accordion;
        new Blog;
        new Forms;
        new GoogleMap('#map');
        new MainMenu;
        new Sliders;
        new Tabs;
        new Video;
        new Popup;

        const audioPlayer = new Plyr('#player', {
            controls: ['play', 'volume']
        }); 

        const videoPlayer = new Plyr( '#video-player', { controls: ['play', 'progress', 'current-time', 'mute', 'volume', 'fullscreen'] });

        $('.c-video').on('click', (e) => {
            let $self = $(e.currentTarget);
            let embedId = $self.data('embed-id');

            videoPlayer.pause();

            videoPlayer.source = {
                type: 'video',
                sources: [
                    {
                        src: embedId,
                        provider: 'youtube',
                    },
                ],
            };

            videoPlayer.on('ready', () => videoPlayer.play());

        });

        this.handlePersonCard();
        this.initCountdown(); 
    }

    initCountdown() {
        $('.c-countdown').each((i, el) => {
            let $self = $(el);

            $self.countdown($self.data('date'), (e) => {
                $self.find('.c-countdown__days').text( e.strftime('%D') );
                $self.find('.c-countdown__hours').text( e.strftime('%H') );
                $self.find('.c-countdown__minutes').text( e.strftime('%M') );
                $self.find('.c-countdown__seconds').text( e.strftime('%S') );
                // $self.html( e.strftime('%D : %H : %M : %S') );
            });
        });
    }

    handlePersonCard() {
        $('.c-card-person__content').on('click', (e) => $(e.currentTarget).parents('.c-card-person').addClass('is-active'));

        $('.c-card-person__description-back').on('click', (e) => $(e.currentTarget).parents('.c-card-person').removeClass('is-active'));
    }
}

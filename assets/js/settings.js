const SETTINGS = {
    sliders: {
        hero: {
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            dots: true,
            centerPadding: '0',
            draggable: false,
            fade: true,
            swipe: false
        },
        feedback: {
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            dots: false,
            centerPadding: '0',
            draggable: false,
            fade: true,
            initialSlide: 1
        },
        teachers: {
            slidesToShow: 4,
            slidesToScroll: 1,
            initialSlide: 2,
            centerMode: true,
            infinite: false,
            arrows: false,
            dots: true,
            draggable: false,
            centerPadding: '0',
            responsive: [
                {
                    breakpoint: 1350,
                    settings: {
                        slidesToShow: 3,
                        initialSlide: 1,
                    }
                },
                {
                    breakpoint: 990,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                        initialSlide: 1,
                    }
                },
                {
                    breakpoint: 720,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        initialSlide: 1,
                        centerMode: true,
                        centerPadding: '30px',
                    }
                }
            ]
        },
        news: {
            slidesToShow: 4,
            slidesToScroll: 1,
            infinite: false,
            arrows: false,
            dots: true,
            centerPadding: '0',
            responsive: [
                {
                    breakpoint: 1500,
                    settings: {
                        slidesToShow: 3,
                        initialSlide: 1,
                    }
                },
                {
                    breakpoint: 990,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                        initialSlide: 1,
                    }
                },
                {
                    breakpoint: 720,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        initialSlide: 0,
                        centerMode: true,
                        centerPadding: '30px',
                    }
                },
                {
                    breakpoint: 500,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        initialSlide: 1,
                        centerMode: true,
                        centerPadding: '35px',
                        arrows: false,
                        dots: false,
                    }
                },

            ]
        },
        tabsMobile: {
            slidesToShow: 4,
            slidesToScroll: 1,
            infinite: false,
            arrows: false,
            dots: false,
            centerPadding: '0',
            responsive: [
                {
                    breakpoint: 400,
                    settings: {
                        slidesToShow: 3,
                    }
                }
            ]
        },
        blocksMobile: {
            slidesToShow: 2,
            slidesToScroll: 1,
            infinite: false,
            arrows: false,
            dots: false,
            centerPadding: '0',
            responsive: [
                {
                    breakpoint: 500,
                    settings: {
                        slidesToShow: 2,
                        centerPadding: '15px',
                        customPaging: '10px',
                    }
                }
            ]
        },
        translation: {
            slidesToShow: 7,
            slidesToScroll: 1,
            infinite: false,
            arrows: false,
            dots: true,
            centerPadding: '0',
            responsive: [
                {
                    breakpoint: 1500,
                    settings: {
                        slidesToShow: 5,
                        initialSlide: 1,
                    }
                },
                {
                    breakpoint: 990,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1,
                        initialSlide: 1,
                    }
                },
                {
                    breakpoint: 720,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                        initialSlide: 0,
                        centerMode: true,
                        centerPadding: '30px',
                    }
                }
            ]
        },
        vacancy: {
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: false,
            arrows: false,
            dots: true,
            centerPadding: '0',
        },
        photos: {
            slidesToShow: 3,
            slidesToScroll: 1,
            centerMode: true,
            infinite: true,
            arrows: false,
            dots: true,
            centerPadding: '0',
            responsive: [
                {
                    breakpoint: 500,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        initialSlide: 1,
                        centerMode: true,
                        centerPadding: '35px',
                        arrows: false,
                        dots: false,
                    }
                }
            ]
        },
    },

    map: {
        styles: [
            {
                "featureType": "water",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#e9e9e9"
                    },
                    {
                        "lightness": 17
                    }
                ]
            },
            {
                "featureType": "landscape",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#f5f5f5"
                    },
                    {
                        "lightness": 20
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#ffffff"
                    },
                    {
                        "lightness": 17
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "color": "#ffffff"
                    },
                    {
                        "lightness": 29
                    },
                    {
                        "weight": 0.2
                    }
                ]
            },
            {
                "featureType": "road.arterial",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#ffffff"
                    },
                    {
                        "lightness": 18
                    }
                ]
            },
            {
                "featureType": "road.local",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#ffffff"
                    },
                    {
                        "lightness": 16
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#f5f5f5"
                    },
                    {
                        "lightness": 21
                    }
                ]
            },
            {
                "featureType": "poi.park",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#dedede"
                    },
                    {
                        "lightness": 21
                    }
                ]
            },
            {
                "elementType": "labels.text.stroke",
                "stylers": [
                    {
                        "visibility": "on"
                    },
                    {
                        "color": "#ffffff"
                    },
                    {
                        "lightness": 16
                    }
                ]
            },
            {
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "saturation": 36
                    },
                    {
                        "color": "#333333"
                    },
                    {
                        "lightness": 40
                    }
                ]
            },
            {
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "transit",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#f2f2f2"
                    },
                    {
                        "lightness": 19
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#fefefe"
                    },
                    {
                        "lightness": 20
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "color": "#fefefe"
                    },
                    {
                        "lightness": 17
                    },
                    {
                        "weight": 1.2
                    }
                ]
            }
        ]
    }
}

export default SETTINGS;

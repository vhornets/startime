import slick from 'slick-carousel';
import SETTINGS from './../settings';

export default class Sliders {
    constructor() {
        this.initSliderHero();
        this.initSliderTeachers();
        this.initSliderNews();
        this.initSliderTranslation();
        this.initSliderFeedback();
        this.initSliderPhotos();
        this.handleSliderArrows();
        this.initSliderVacancy();
        this.tabsMobile();
        this.blocksMobile();
    }

    initSliderHero() {
        let changeImage = (e, slick, currentSlide, nextSlide) => {
            $('.c-slider-hero__image').fadeOut('fast', () => {
                setTimeout(() => $('.c-slider-hero__image').eq(nextSlide).fadeIn(), 300);
            });
        };

        $('.c-slider-hero')
            .slick(SETTINGS.sliders.hero)
            .on('beforeChange', changeImage)
        ;
    }

    initSliderTeachers() {
        let initCenterPadding = function(e, slick) {
            if($(window).width() <= 1110) return;

            let $slider = $(this);
            let $slider_title = $slider.parents('section').find('.c-slider-teachers__title');
            let slider_title_offset = $slider_title.offset().left - 20;

            setTimeout(() => $slider.slick('slickSetOption', 'centerPadding', `${slider_title_offset}px`), 100);
        }

        let setInitialSlide = function(e, slick) {
            if($(window).width() <= 990) return;

            var $slider = $(this);
            var min_slides_number = 3;

            if(slick.$slides.length <= min_slides_number) {
                $slider.addClass('has-few-slides');
            }

            if(slick.$slides.length == 1) {
                $slider.addClass('has-one-slide');
            }
        };

        $('.c-slider-teachers')
            .on('init', initCenterPadding)
            .on('breakpoint', initCenterPadding)
            .on('init', setInitialSlide)
            .slick(SETTINGS.sliders.teachers)
        ;
    }

    initSliderNews() {
        $('.c-slider-news').slick(SETTINGS.sliders.news);
    }

    tabsMobile() {
        $('.c-slider-tabs-mobile').slick(SETTINGS.sliders.tabsMobile);
    }

    blocksMobile() {
        $('.c-slider-blocks-mobile').slick(SETTINGS.sliders.blocksMobile);
    }

    initSliderVacancy() {
        $('.c-slider-vacancy').slick(SETTINGS.sliders.vacancy);
    }

    initSliderTranslation() {
        $('.c-slider-translation').slick(SETTINGS.sliders.translation);
    }

    initSliderFeedback() {
        let changeImage = (e, slick, currentSlide, nextSlide) => {
            let $photos = slick.$slider.parents('.js-tab').find('.c-slider-feedback__photos-set');

            $photos.fadeOut('fast', () => {
                setTimeout(() => $photos.eq(nextSlide).fadeIn(), 300);
            });
        };

        $('.c-slider-feedback').each((i, el) => {
            $(el)
                .on('beforeChange', changeImage)
                .slick(SETTINGS.sliders.feedback)
            ;
        });
    }

    initSliderPhotos() {
        $('.c-slider-photos')
            .on('init', (e, slick, currentSlide, nextSlide) => {
                let $currentSlide = slick.$slider.find('.slick-current');

                $('.c-slider-photos__arrow--prev').offset({
                    left: $currentSlide.offset().left - 25
                });

                $('.c-slider-photos__arrow--next').offset({
                    left: $currentSlide.offset().left + $currentSlide.width() - 50 + 25
                });
                $('body').on('click', '.slick-active-tab', function () {
                    console.log('test')
                    console.log(this);
                    let tabindex = !!this.getAttribute('tabindex');
                    let sliderId = this.querySelector('.c-slider-photos__photo').getAttribute('data-slider');

                    if (tabindex) {
                        $( sliderId ).slick( 'slickPrev' );
                    } else if (!tabindex && !this.classList.contains('slick-center')) {
                        $( sliderId ).slick( 'slickNext' );
                    }
                })
            })
            .slick(SETTINGS.sliders.photos)
        ;
    }

    handleSliderArrows() {
        $('.js-change-slide').on('click', (e) => {
            e.preventDefault();

            let $self = $(e.currentTarget);

            if ($self.data('action') === 'slickGoTo') {
                $( $self.data('slider') ).slick( $self.data('action'), $self.data('key') );
            } else {
                $( $self.data('slider') ).slick( $self.data('action') );
            }
        });
    }
}

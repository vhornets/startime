import validate from 'jquery-validation';
import 'jquery.inputmask';

export default class Forms {
    constructor() {
        this.initInputMask();
        this.initFormValidation();
        this.handleFormSubmit();
        this.handleVote();
    }

    initInputMask() {
         $('input[type="tel"]').inputmask("+38(999) 999-99-99");
         $('input[name="birthday"]').inputmask({
            mask: "99-99-9999",
            placeholder: "дд-мм-гггг"
         });
    }

    initFormValidation() {
        $('.js-form-validate').each(function() {
            $(this).validate({
                errorPlacement: () => false,

                submitHandler: function(form) {
                    $(form).find('.c-dropdown__selected.error').removeClass('error');

                    $(form).trigger('form.valid');

                    return false;
                }
            });
        });
    }

    handleFormSubmit() {
        $('.js-form-submit').on('form.valid', function() {
            var $self = $(this);
            var $button = $self.find('button[type="submit"]');
            var $responseModal = $('[data-remodal-id="modal-response"]');
            var button_old_text = $button.text();

            $button.text( $button.data('submitting-text') );

            var request = $.ajax('/wp-admin/admin-ajax.php', {
                method: 'post',
                data: $self.serialize()
            });

            request.done((res) => {
                $button.text(button_old_text);

                $self.get(0).reset();

                $responseModal.find('.c-modal__content').text(res);

                $responseModal.remodal().open();

                setTimeout(() => $responseModal.remodal().close(), 3000);
            });
        });
    }

    handleVote() {
        $('.js-nominee-vote-form').on('submit', (e) => {
            e.preventDefault();

            let $form = $(e.currentTarget);
            let $button = $form.find('[type="submit"]');
            let buttonOldText = $button.text();

            $button.text( $button.data('submitting-text') );

            var request = $.ajax('/wp-admin/admin-ajax.php', {
                method: 'post',
                data: $form.serialize()
            });

            request.done((res) => {
                $button.text( $button.data('voted-text') );

                $form.parent().find('.c-card-person__votes-value').text(res);

                $('.js-nominee-vote-form').each((i, el) => {
                    let $self = $(el);

                    $self.find('[type="submit"]').addClass('is-disabled');
                });
            });

            return false; 
        });
    }
}

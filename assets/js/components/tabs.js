export default class Tabs {
    constructor() {
        this.handleTabsClick();

        $('.js-camp-shift-tab').on('tab.shown', (e) => {
            let $activeTab = $(e.currentTarget);

            $('.schedule__shift').hide();
            $('.schedule__shift').eq( $activeTab.index() ).show();
        });
    }

    handleTabsClick() {
        $(document).on('click', '.js-toggle-tab', function(e, data) {
            e.preventDefault();

            var $self = $(this);
            var $target = $( $self.attr('href') );
            var $tabs_container = $target.parent();
            var $tabs = $tabs_container.find('> .js-tab');
            var $tabs_nav = $self.parents('.js-tabs-nav');

            $tabs_nav.find('.js-toggle-tab').removeClass('is-active');
            $self.addClass('is-active');

            $tabs.removeClass('is-active');
            $target.addClass('is-active');

            if(!data) { 
                $target.trigger('tab.shown');
                $('body').trigger('tab.changed');
            }

            if($tabs_nav.hasClass('c-tabs-nav-big')) {
                var $firstTabLink = $tabs_nav.find('li').first();
                var firstTabLinkHtml = $firstTabLink.html();

                $firstTabLink.html( $self.parent().html() );
                $self.parent().html(firstTabLinkHtml);
            }
        });

        var activateInitialTab = () => {
            $('.js-toggle-tab.is-init').each(function() { 
                $(this).trigger('click').removeClass('is-init');
            });
        }

        activateInitialTab();

        $(window).on('tabs.loaded', activateInitialTab);
    }
}

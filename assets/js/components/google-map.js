import GMaps from 'gmaps';
import SETTINGS from '../settings';

export default class GoogleMap {
    constructor(id) {
        this.id = id;
        this.$map = $(id);
        this.mapMarkers = [];

        this.mapMarkers.push(this.$map.data('map-marker'));

        this.loadGoogleMapsApi();
    }

    loadGoogleMapsApi() {
        var lang = 'en';

        if ($('html').attr('lang')) {
            lang = $('html').attr('lang');
        }

        var js_file = document.createElement('script');

        js_file.type = 'text/javascript';
        js_file.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyCadoAMTROrTEgmNnBOJFO9N-Ld3EwJS6Q&language=' + lang;
        document.getElementsByTagName('head')[0].appendChild(js_file);

        js_file.onload = () => this.init();
    }

    init() {
        this.initMap();
    }

    initMap() {
        var markers_locations = [];

        var map_atts = {
            div: this.id,
            scaleControl: false,
            zoomControl: false,
            fullscreenControl: false,
            mapTypeControl: false,
            scrollwheel: false,
            streetViewControl: false,
            zoom: 15,
            lat: this.mapMarkers[0].lat,
            lng: this.mapMarkers[0].lng
        };

        var map = new GMaps(map_atts);


        map.addStyle({
            styledMapName:"Styled Map",
            styles: SETTINGS.map.styles,
            mapTypeId: "map_style"
        });

        map.setStyle("map_style");

        $.each(this.mapMarkers, (i, elem) => {
            var marker = map.addMarker({
                lat: elem.lat,
                lng: elem.lng,
                // icon: this.$map.data('map-marker-icon')
            });

            markers_locations.push(
                new google.maps.LatLng(elem.lat, elem.lng)
            );
        });

        if (this.mapMarkers.length != 1) {
            map.fitLatLngBounds(markers_locations);
            map.fitZoom(markers_locations);
        }

        else {
            map.setZoom(17);
        }
    }
}

export default class Blog {
    constructor() {
        this.handleLoadMoreButton();
    }

    handleLoadMoreButton() {
        $(document).on('click', '.js-load-next-posts-page', (e) => {
            e.preventDefault();

            let $self = $(e.currentTarget);
            let request = $.ajax( $self.attr('href'), { method: 'post' } );

            $self.text( $self.data('loading-text') );

            request.done((res) => {
                let $res = $('<div></div>').html(res);
                let $posts = $res.find('.js-blog-posts');
                let $pagination = $res.find('.js-blog-pagination-container');

                $('.js-blog-posts').append( $posts.html() );
                $('.js-blog-pagination-container').html( $pagination.html() );
            });
        });
    }
}

export default class Accordion {
    constructor() {
        this.tabs = document.querySelectorAll('.js-accordion-tab');
        this.initAccordionTab();
    }

    openTab(tab) {
        tab.classList.add('is-active');
    }

    closeTab(tab) {
        tab.classList.remove('is-active');
    }

    closeAllTabs() {
        if (this.tabs.length) {
            this.tabs.forEach(tab => {
                this.closeTab(tab);
            })
        }
    }

    initAccordionTab () {
        if (this.tabs.length) {
            let _this = this;
            this.tabs.forEach(tab => {
                tab.querySelector('.c-section-faq__single--header').addEventListener('click', function () {
                    if (tab.classList.contains('is-active')) {
                        _this.closeTab(tab);
                    } else {
                        _this.closeAllTabs();
                        _this.openTab(tab);
                    }
                })
            })
        }
    }
}

const bodyScrollLock = require('body-scroll-lock');
const disableBodyScroll = bodyScrollLock.disableBodyScroll;
const enableBodyScroll = bodyScrollLock.enableBodyScroll;

export default class MainMenu {
    constructor() {
        this.menuMobileToggle();
        // this.menuMobileSubmenuToggle();
        // this.menuMobileSetHeadersToSubmenu();
    }

    menuMobileToggle() {
        $('.js-toggle-main-menu').on('click', (e) => {
            e.preventDefault();

            $('body').toggleClass('menu--open');
            $('html').toggleClass('menu--open');

            if($('.sub-menu.is-shown').length) {
                $('.sub-menu.is-shown').removeClass('is-shown');

                $('.c-main-menu-mobile-contacts').fadeIn();

                return;
            }

            $('.c-main-menu-mobile__wrap').fadeToggle();

            let body = document.querySelector('body');

            if ($('body').hasClass('menu--open')) {
                disableBodyScroll(body);
            } else {
                enableBodyScroll(body)
            }

        });

        function preventDefault(e){
            e.preventDefault();
        }

        function disableScroll(){
            document.body.addEventListener('touchmove', preventDefault, { passive: false });
        }
        function enableScroll(){
            document.body.removeEventListener('touchmove', preventDefault);
        }
    }

    // menuMobileSubmenuToggle() {
    //     $('.c-main-menu-mobile .menu-item-has-children > a').on('click', function(e) {
    //         e.preventDefault();
    //
    //         $(this).parent().find('.sub-menu').addClass('is-shown');
    //
    //         $('.c-main-menu-mobile-contacts').fadeOut();
    //     });
    // }
    //
    // menuMobileSetHeadersToSubmenu() {
    //     $('.c-main-menu-mobile .menu-item-has-children').each((i, el) => {
    //         let $self = $(el);
    //         let $submenu = $self.find('.sub-menu');
    //         let $link = $self.find('> a');
    //         let linkText = $link.text();
    //         let linkHref = $link.attr('href');
    //
    //         $submenu.prepend(`<li class="sub-menu-title"><a href="${linkHref}">${linkText}</a></li>`);
    //     });
    // }
}

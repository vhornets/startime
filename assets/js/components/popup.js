export default class Popup {
    constructor() {
        this.html = document.querySelector('html');
        this.body = document.querySelector('body');
        this.popup = document.getElementById('popup');
        this.sectionInput = this.popup.querySelector('input[name="section"]');

        if (typeof this.popup !== "undefined") {
            this.openPopup();
            this.closePopup();
        } 
    }

    openPopup() {
        let _this = this;
        let buttons = this.body.querySelectorAll('.open-popup');

        if (buttons.length) {
            for(let item of buttons) {
                item.addEventListener('click', function (e) {
                    e.preventDefault();
                    let type = this.getAttribute('data-open');

                    _this.html.classList.add('popup--open');
                    _this.body.classList.add('popup--open');
                    _this.popup.classList.add('popup--open');
                    _this.popup.setAttribute('data-open', type);
                    _this.sectionInput.value = item.getAttribute('data-section');
                })
            }
        }
    }

    closePopup() {
        let _this = this;
        let button = this.popup.querySelector('.popup__close');

        if (button) {
            button.addEventListener('click', function () {
                _this.popup.classList.remove('popup--open');
                _this.body.classList.remove('popup--open');
                _this.html.classList.remove('popup--open');
                _this.sectionInput.value = '';
            })
        }
    }
}
